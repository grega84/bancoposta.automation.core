package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;


import android.Utility;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.VoucherVerificationBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import it.poste.bancoposta.pages.BonificoConfirmSubPage;
import it.poste.bancoposta.pages.InsertPosteIDPage;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.OKOperationPage;
import it.poste.bancoposta.pages.VoucherSubscribePage;
import test.automation.core.UIUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="VoucherSubscribePage")
@Android
@IOS
public class VoucherSubscribePageMan extends LoadableComponent<VoucherSubscribePageMan> implements VoucherSubscribePage
{
	UiPage page;

	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement header;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement backButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement title;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement description;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement typeInfo;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement subscribeWithInfo;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement ownerInfo;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement amountInfo;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement checkBoxInfo;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement subscribe ;
	private VoucherVerificationBean bean;
	private InsertPosteIDPage posteIdInsert;
	private OKOperationPage tnkPage;

	
	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
		page.get();
		
		this.typeInfo=page.getParticle(Locators.VoucherSubscribePageMolecola.TYPEINFO).getElement();
		this.subscribeWithInfo=page.getParticle(Locators.VoucherSubscribePageMolecola.SUBSCRIBEWITHINFO).getElement();
		this.ownerInfo=page.getParticle(Locators.VoucherSubscribePageMolecola.OWNERINFO).getElement();
		this.amountInfo=page.getParticle(Locators.VoucherSubscribePageMolecola.AMOUNTINFO).getElement();

		Assert.assertTrue("controllo tipo buono; attuale:"+this.typeInfo.getText().trim()+", attesa:"+bean.getVoucherType(),this.typeInfo.getText().trim().equals(bean.getVoucherType()));
		Assert.assertTrue("controllo sottoscrivi con; attuale:"+this.subscribeWithInfo.getText().trim()+", atteso:"+bean.getPayWith().replace(";", " "),this.subscribeWithInfo.getText().toLowerCase().trim().equals(bean.getPayWith().replace(";", " ")));

		int actualAmuount=(int) Double.parseDouble(this.amountInfo.getText().trim().replace("\u20AC", "").replace(",", ".").replace(" ", ""));
		int expectedAmount=(int) Double.parseDouble(bean.getAmount());

		Assert.assertTrue("controllo importo; attuale:"+this.amountInfo.getText().trim().replace("\u20AC", "").replace(",", ".").replace(" ", "")+", attesa:"+bean.getAmount(),actualAmuount == expectedAmount);
		Assert.assertTrue("controllo sottoscrivente",this.ownerInfo.getText().trim().equals(bean.getSender()));

		Utility.swipe((AppiumDriver<?>)page.getDriver(), Utility.DIRECTION.DOWN, 200);

		page.getParticle(Locators.VoucherSubscribePageMolecola.CHECKBOXINFO).visibilityOfElement(10L);
		page.getParticle(Locators.VoucherSubscribePageMolecola.SUBSCRIBE ).visibilityOfElement(10L);


		}

	public void subscribe(String posteId) 
	{
		this.checkBoxInfo=page.getParticle(Locators.VoucherSubscribePageMolecola.CHECKBOXINFO).getElement();
		this.subscribe=page.getParticle(Locators.VoucherSubscribePageMolecola.SUBSCRIBE).getElement();
		

		Utility.swipe((MobileDriver) page.getDriver(), Utility.DIRECTION.DOWN, 200);

		this.checkBoxInfo.click();
		this.subscribe.click();

		this.posteIdInsert=(InsertPosteIDPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("InsertPosteIDPage", page.getDriver().getClass(), page.getLanguage());

		this.posteIdInsert.checkPage();

		this.tnkPage=this.posteIdInsert.insertPosteID(posteId);

		this.tnkPage.close();
	}

	public void back() 
	{
		this.backButton=page.getParticle(Locators.VoucherSubscribePageMolecola.BACKBUTTON).getElement();
		

		this.backButton.click();
	}

	@Override
	public void init(UiPage page) {
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) {
		load();
	}
}
