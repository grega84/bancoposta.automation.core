package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import automation.core.ui.uiobject.UiPage;
import it.poste.bancoposta.pages.BonificoConfirmSubPage;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.PayWithPage;
import it.poste.bancoposta.pages.RicaricaPostepayPage;
import test.automation.core.UIUtils;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="PayWithPage")
@Android
@IOS
public class PayWithPageMan extends LoadableComponent<PayWithPageMan> implements PayWithPage
{
	UiPage page;

	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement header;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement cancellButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement title;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement productList;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement productElement;
	/**
	 * @android 
	 * @ios campo dinamico sostituire $(index) con l'indice dell'elemento da ricercare
	 * @web 
	 */
	private WebElement productDetailType;
	/**
	 * @android 
	 * @ios campo dinamico sostituire $(index) con l'indice dell'elemento da ricercare
	 * @web 
	 */
	private WebElement productAmount;
	/**
	 * @android 
	 * @ios campo dinamico sostituire $(index) con l'indice dell'elemento da ricercare
	 * @web 
	 */
	private WebElement productType;

	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
		page.get();
	}

	public void clickOnConto(String payWith) 
	{
		String element="";
		String title="";
		String detail="";

		element=page.getParticle(Locators.PayWithPageMolecola.PRODUCTELEMENT).getLocator();
		title=page.getParticle(Locators.PayWithPageMolecola.PRODUCTTYPE).getLocator();
		detail=page.getParticle(Locators.PayWithPageMolecola.PRODUCTDETAILTYPE).getLocator();

		String[] payWithInfo=payWith.split(";");

		List<WebElement> list=page.getDriver().findElements(By.xpath(element));

		for(WebElement e : list)
		{
			String titleTxt=e.findElement(By.xpath(title)).getText().trim().toLowerCase();
			String descrTxt=e.findElement(By.xpath(detail)).getText().trim().toLowerCase(); 
			String fourDigits = descrTxt.substring(descrTxt.length() - 4);
			if(payWithInfo[0].equals(titleTxt) && payWithInfo[1].contains(fourDigits))
			{descrTxt.substring(descrTxt.length() - 4);
			e.click();
			break;
			}
		}
	}

	public void clickBack() 
	{
		page.getDriver().getPageSource();
		
		this.cancellButton=page.getParticle(Locators.PayWithPageMolecola.CANCELLBUTTON).getElement();
		

		this.cancellButton.click();

	}

	public RicaricaPostepayPage clickOnContodue(String payWith) 
	{  
		String element="";
		String title="";
		String detail="";

		element=page.getParticle(Locators.PayWithPageMolecola.CARDELEMENT).getLocator();
		title=page.getParticle(Locators.PayWithPageMolecola.PRODUCTTYPE).getLocator();
		detail=page.getParticle(Locators.PayWithPageMolecola.PRODUCTDETAILTYPE).getLocator();
		
		System.out.println(title);

		String[] payWithInfo=payWith.split(";");
		System.out.println(Arrays.toString(payWithInfo));

		System.out.println(page.getDriver().getPageSource());

		List<WebElement> list=page.getDriver().findElements(By.xpath(element));

		for(WebElement e : list)
		{
			System.out.println(e);
			String titleTxt=e.findElement(By.xpath(title)).getText().trim().toLowerCase();
			String descrTxt=e.findElement(By.xpath(detail)).getText().trim().toLowerCase(); 
			String fourDigits = descrTxt.substring(descrTxt.length() - 4);
			if(payWithInfo[0].equals(titleTxt) && payWithInfo[1].contains(fourDigits))
			{descrTxt.substring(descrTxt.length() - 4);
			e.click();
			break;
			}
		}
		
		RicaricaPostepayPage p = (RicaricaPostepayPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("RicaricaPostepayPage", page.getDriver().getClass(), page.getLanguage());;
		p.checkPage();
		return p;
	}

	@Override
	public void init(UiPage page) {
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) {
		load();
	}

	@Override
	public void clikOnOkButton() {
		WebElement OkOkButton = page.getParticle(Locators.PayWithPageMolecola.OKKOBUTTON).getElement();
		OkOkButton.click();
	}

}
