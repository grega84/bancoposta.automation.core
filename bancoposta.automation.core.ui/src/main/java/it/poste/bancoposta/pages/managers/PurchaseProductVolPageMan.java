package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.TutorialPopUpBean;
import io.appium.java_client.AppiumDriver;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.PurchaseProductVolPage;
import test.automation.core.UIUtils;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="PurchaseProductVolPage")
@Android
@IOS
public class PurchaseProductVolPageMan extends LoadableComponent<PurchaseProductVolPageMan> implements PurchaseProductVolPage
{
	UiPage page;

	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
		page.get();
	}

	public void clickContinua() 
	{
		page.getParticle(Locators.PurchaseVolRequestMolecola.CONTINUA_BUTTON).visibilityOfElement(10L).click();
	}

	@Override
	public void init(UiPage page) {
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) {
		load();
	}

	

}
