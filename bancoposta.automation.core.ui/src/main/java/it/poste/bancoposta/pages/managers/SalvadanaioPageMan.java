package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import io.appium.java_client.HidesKeyboard;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.CredentialAccessBean;
import bean.datatable.SalvadanaioDataBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.android.AndroidDriver;
import it.poste.bancoposta.pages.InsertPosteIDPage;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.ModifyObjectivePage;
import it.poste.bancoposta.pages.SalvadanaioCreateObjData;
import it.poste.bancoposta.pages.SalvadanaioCreateObjImporto;
import it.poste.bancoposta.pages.SalvadanaioCreateObjNome;
import it.poste.bancoposta.pages.SalvadanaioCreateObjRiepilogo;
import it.poste.bancoposta.pages.SalvadanaioInfoObjectivePage;
import it.poste.bancoposta.pages.SalvadanaioPage;

import test.automation.core.UIUtils;

import java.sql.Date;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="SalvadanaioPage")
@Android
@IOS
public class SalvadanaioPageMan extends LoadableComponent<SalvadanaioPageMan> implements SalvadanaioPage
{
	UiPage page;

	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement header;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement backButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement helpButton;


	private WebElement labelPage;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	private WebElement description;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	private WebElement menuButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */


	private WebElement firstAmount;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	private WebElement secondAmount;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	private WebElement expireDate;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	private WebElement arrowButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	private WebElement createNewTargetButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	private List<WebElement> record;
	private WebElement objectiveName;
	private WebElement titolo;
	private WebElement image;
	private WebElement data;
	private WebElement confirm;
	private WebElement amount;
	private WebElement buttonok;
	private WebElement nome;
	private WebElement clickon;
	private WebElement controllotext;
	private WebElement okpopup;
	private String oktext;
	private WebElement createNewTarget;
	private WebElement amount2;
	private WebElement date;
	private WebElement dt_param;
	private WebElement icon;
	private WebElement cronometro;

	private WebElement modifyObjective;

	private WebElement menu;

	private WebElement chiudiSalvadanaio;


	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	@Override
	protected void isLoaded() throws Error {

	} 

	@Override
	protected void load() 
	{
		page.get();
	}

	public SalvadanaioInfoObjectivePage chooseObjective(String objectiveName) {


		String title="";

		title = page.getParticle(Locators.SalvadanaioPageMolecola.DESCRIPTION).getLocator();


		String obiettivo= objectiveName.trim().toLowerCase();

		List<WebElement> list=page.getDriver().findElements(By.xpath(title));


		for(WebElement e : list)
		{
			String titleTxt=e.findElement(By.xpath(title)).getText().trim().toLowerCase();
			if(obiettivo.equals(titleTxt))
			{
				e.click();
				break;
			}
		}

		SalvadanaioInfoObjectivePage c = (SalvadanaioInfoObjectivePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SalvadanaioInfoObjectivePage", page.getDriver().getClass(), page.getLanguage());
		c.checkPage();

		return c;
	}


	public void checkCronometro(String objectiveName) {

		String title="";
		title = page.getParticle(Locators.SalvadanaioPageMolecola.DESCRIPTION).getLocator();

		String obiettivo= objectiveName.trim().toLowerCase();
		List<WebElement> list=page.getDriver().findElements(By.xpath(title));
		for(WebElement e : list)
		{
			String titleTxt=e.findElement(By.xpath(title)).getText().trim().toLowerCase();
			if(obiettivo.equals(titleTxt))
			{
				try {
					cronometro = page.getParticle(Locators.SalvadanaioPageMolecola.CRONOMETROICON).getElement();

				} catch (Exception r) {
					System.out.println("L'icona cronometro � correttamente non visualizzata");
				}
				e.click();
				break;
			}
		}
		SalvadanaioInfoObjectivePage c = (SalvadanaioInfoObjectivePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SalvadanaioInfoObjectivePage", page.getDriver().getClass(), page.getLanguage());
		c.checkPage();

		try {
			cronometro = page.getParticle(Locators.SalvadanaioInfoPageMolecola.CRONOMETROICONINFO).getElement();

		} catch (Exception r) {
			System.out.println("L'icona cronometro � correttamente non visualizzata");
		}

		backButton = page.getParticle(Locators.SalvadanaioInfoPageMolecola.BACKBUTTON).getElement();

		this.backButton.click();

	}

	public SalvadanaioInfoObjectivePage chooseObjectiveTwo(String objectiveNameNew) {

		String title="";

		title = page.getParticle(Locators.SalvadanaioPageMolecola.DESCRIPTION).getLocator();


		String obiettivo= objectiveNameNew.trim().toLowerCase();

		//TODO:TAA-322
		for(int i=0;i<2;i++)
		{
			List<WebElement> list=page.getDriver().findElements(By.xpath(title));

			System.out.println("salvadanaio da aprire: "+obiettivo);

			for(WebElement e : list)
			{
				String titleTxt=e.findElement(By.xpath(title)).getText().trim().toLowerCase();
				System.out.println("controllo salvadanaio "+titleTxt);

				if(obiettivo.equals(titleTxt))
				{
					e.click();
					break;
				}
			}
			try {
				UIUtils.mobile().swipe((MobileDriver) page.getDriver(), UIUtils.SCROLL_DIRECTION.DOWN, 1000);
				WaitManager.get().waitMediumTime();
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

		WaitManager.get().waitMediumTime();
		//System.out.println(driver.getPageSource());
		SalvadanaioInfoObjectivePage c = (SalvadanaioInfoObjectivePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SalvadanaioInfoObjectivePage", page.getDriver().getClass(), page.getLanguage());
		c.checkPage();

		return c;
	}

	//	public void clickOnConto(String payWith) 
	//	{
	//		String element="";
	//		String title="";
	//		String detail="";
	//
	//		switch(this.driverType)
	//		{
	//		case 0://android driver
	//			element=page.getParticle(Locators.PayWithPageLocator.PRODUCTELEMENT).getLocator();
	//			title=Locators.PayWithPageLocator.PRODUCTTYPE.getLocator(driver);
	//			detail=Locators.PayWithPageLocator.PRODUCTDETAILTYPE.getLocator(driver);
	//			break;
	//		case 1://ios driver
	//			element=Locators.PayWithPageLocator.PRODUCTELEMENT.getLocator(driver);
	//			title=Locators.PayWithPageLocator.PRODUCTTYPE.getLocator(driver);
	//			detail=Locators.PayWithPageLocator.PRODUCTDETAILTYPE.getLocator(driver);
	//			break;
	//		case 2://web driver
	//
	//			break;
	//		}
	//
	//		String[] payWithInfo=payWith.split(";");
	//
	//		List<WebElement> list=driver.findElements(By.xpath(element));
	//
	//		for(WebElement e : list)
	//		{
	//			String titleTxt=e.findElement(By.xpath(title)).getText().trim().toLowerCase();
	//			String descrTxt=e.findElement(By.xpath(detail)).getText().trim().toLowerCase(); 
	//			String fourDigits = descrTxt.substring(descrTxt.length() - 4);
	//			if(payWithInfo[0].equals(titleTxt) && payWithInfo[1].contains(fourDigits))
	//			{descrTxt.substring(descrTxt.length() - 4);
	//			e.click();
	//			break;
	//			}
	//		}
	//	}



	public void createNewTargetButton() 
	{
		UIUtils.mobile().swipe((MobileDriver) page.getDriver(), UIUtils.SCROLL_DIRECTION.DOWN, 500);
		WaitManager.get().waitShortTime();
		this.titolo = page.getParticle(Locators.SalvadanaioPageMolecola.CREATENEWTARGETBUTTON).getElement();

		this.titolo.click();
	}

	public void categoryImage() 
	{
		this.image = page.getParticle(Locators.SalvadanaioPageMolecola.CATEGORYIMAGEICON).getElement();

		this.image.click();
	}
	public void insertData(List<SalvadanaioDataBean> b) 
	{

		SalvadanaioDataBean c = b.get(0);
		b.get(0);
		
		SalvadanaioCreateObjData p = (SalvadanaioCreateObjData) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SalvadanaioCreateObjData", page.getDriver().getClass(), page.getLanguage());
		
		p.checkPage();
		
		p.selectData(b);
		
		p.checkDataIsSelected();
		
		p.clickAvanti();
	}

	public void insertImporto(List<SalvadanaioDataBean> b) 
	{

		SalvadanaioDataBean c = b.get(0);
		//b.get(0);
		
		SalvadanaioCreateObjImporto p = (SalvadanaioCreateObjImporto) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SalvadanaioCreateObjImporto", page.getDriver().getClass(), page.getLanguage());
		
		p.checkPage();
		
		p.insertImporto(c);
		
		p.clickAvanti();
	}

	public void insertNome(List<SalvadanaioDataBean> b) {

		SalvadanaioDataBean c = b.get(0);
		//b.get(0);
		SalvadanaioCreateObjNome p = (SalvadanaioCreateObjNome) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SalvadanaioCreateObjNome", page.getDriver().getClass(), page.getLanguage());
		
		p.checkPage();
		
		p.insertNome(c);
		
		p.clickAvanti();
	}

	public void controlloPopup() {
		//		 SummaryTargetPage s = new SummaryTargetPage(driver, driverType);
		//	        s.get();
		this.controllotext = page.getParticle(Locators.SalvadanaioPageMolecola.TEXTCONTENTPOPUP).getElement();
		this.okpopup = page.getParticle(Locators.SalvadanaioPageMolecola.OKBUTTONPOPUP).getElement();

		this.oktext = controllotext.getText().trim();
		System.out.println(oktext);
		Assert.assertTrue(oktext.equals("L'importo massimo del Salvadanaio e' di 5000 euro. Per proseguire, modifica l'importo."));
		this.okpopup.click();
	}

	public void createObjective() 
	{	

		UIUtils.mobile().swipe((MobileDriver) page.getDriver(), UIUtils.SCROLL_DIRECTION.DOWN, 0.5f, 0.8f, 0.5f, 0.5f, 1000);

		this.createNewTarget= page.getParticle(Locators.SalvadanaioPageMolecola.CREATENEWTARGETBUTTON).getElement();


		this.createNewTarget.click();	 
	}

	public void createObjective(String objectiveName, String posteID,String evolutionNumber,String owner) 
	{	

		UIUtils.mobile().swipe((MobileDriver) page.getDriver(), UIUtils.SCROLL_DIRECTION.DOWN, 0.5f, 0.8f, 0.5f, 0.5f, 1000);

		try {
			this.createNewTarget= page.getParticle(Locators.SalvadanaioPageMolecola.CREATENEWTARGETBUTTON).getElement();

		} catch (Exception e) {
			System.out.println("E' stato raggiunto il limite di 5 Obiettivi - Si sta procedendo alla chiusura dell'obiettivo: "+" "+ objectiveName);
			System.out.println(objectiveName);
			chooseObjective(objectiveName);

			SalvadanaioInfoObjectivePage c = (SalvadanaioInfoObjectivePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("SalvadanaioInfoObjectivePage", page.getDriver().getClass(), page.getLanguage());
			c.checkPage();
			c.closeObjective(posteID,evolutionNumber,owner);

			try { 
				chooseObjective(objectiveName);
			} catch (Exception r) {
				System.out.println("L'obiettivo � stato eliminato correttamente");
			}

			this.createNewTarget= page.getParticle(Locators.SalvadanaioPageMolecola.CREATENEWTARGETBUTTON).getElement();


		}

		this.createNewTarget.click();	 
	}


	public void checkObjective(String objectiveName) 
	{	
		String title2="";

		title2= page.getParticle(Locators.SalvadanaioPageMolecola.DESCRIPTION).getLocator();

		String obiettivo= objectiveName.trim().toLowerCase();

		List<WebElement> list=page.getDriver().findElements(By.xpath(title2));

		boolean found=false;

		for(WebElement e : list)
		{   
			String titleTxt=e.findElement(By.xpath(title2)).getText().trim().toLowerCase();

			if(obiettivo.equals(titleTxt))
			{
				System.out.println("Il tuo obiettivo � stato creato"+ obiettivo +"Obiettivo visualizzato sull app"+ titleTxt);
				found=true;
				break;
			} 			 		 
		}
		Assert.assertTrue(found); 
	}



	public void checkObjectiveTwo(String objectiveNameNew, String importo2) 
	{	

		String title3="";

		title3= page.getParticle(Locators.SalvadanaioPageMolecola.DESCRIPTION).getLocator();
		amount2= page.getParticle(Locators.SalvadanaioPageMolecola.SECONDAMOUNT).getElement();
		
		String obiettivo= objectiveNameNew.trim().toLowerCase();

		List<WebElement> list=page.getDriver().findElements(By.xpath(title3));

		boolean found=false;

		for(WebElement e : list)
		{   
			String titleTxt=e.findElement(By.xpath(title3)).getText().trim().toLowerCase();

			if(obiettivo.equals(titleTxt))
			{
				System.out.println("Il tuo obiettivo � stato creato"+ obiettivo +"Obiettivo visualizzato sull app"+ titleTxt);
				found=true;
				break;
			} 			 		 
		}
		Assert.assertTrue(found); 


		String a = amount2.getText();
		System.out.println(a);
		a =a.replace("/ � ", "").trim();
		System.out.println(a);
		org.springframework.util.Assert.isTrue(a.equals(importo2));
		System.out.println(a+" "+importo2);

	}

	public void checkDate(String dt){

		dt_param= page.getParticle(Locators.SalvadanaioPageMolecola.EXPIREDDATE).getElement();
		String dtt = dt_param.getText().replace("Scade il ","").trim();
		System.out.println(dtt);
		System.out.println(dt);
		org.springframework.util.Assert.isTrue(dtt.equals(dt));

	}

	public void goToHomepage() {

		this.backButton= page.getParticle(Locators.SalvadanaioPageMolecola.BACKBUTTON).getElement();
		

		this.backButton.click();


	}

	@Override
	public void init(UiPage page) {
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) {
		load();
	}

	@Override
	public void checkModificaObiettivo(SalvadanaioDataBean b) {
		menu =page.getParticle(Locators.SalvadanaioPageMolecola.MENU).getElement();	
		menu.click();	

		modifyObjective =page.getParticle(Locators.SalvadanaioPageMolecola.MODIFYOBJECTIVE).getElement();	
		modifyObjective.click();	

		ModifyObjectivePage m= (ModifyObjectivePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("ModifyObjectivePage", page.getDriver().getClass(), null);	
		m.checkPage();	


		String s2 =m.modifyObjective(b.getObjectiveNameNew(),b.getCategory(),b.getImporto(),b.getImporto2());  	
		checkPage();	
		checkDate(s2);
	}

	@Override
	public void clickChiudi() {
		this.chiudiSalvadanaio=page.getParticle(Locators.SalvadanaioPageMolecola.LEFTMENU).getElement();		
		chiudiSalvadanaio.click();	
	}

	@Override
	public SalvadanaioCreateObjRiepilogo checkRiepilogoCreazione(List<SalvadanaioDataBean> b) 
	{
		SalvadanaioDataBean c = b.get(0);
		//b.get(0);
		SalvadanaioCreateObjRiepilogo p = (SalvadanaioCreateObjRiepilogo) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SalvadanaioCreateObjRiepilogo", page.getDriver().getClass(), page.getLanguage());
		
		p.checkPage(c);
		
		return p;
	}




}
