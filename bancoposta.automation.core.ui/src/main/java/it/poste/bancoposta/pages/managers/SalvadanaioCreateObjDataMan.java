package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import automation.core.ui.uiobject.UiPage;
import automation.core.ui.uiobject.UiPageRepository;
import bean.datatable.CredentialAccessBean;
import bean.datatable.SalvadanaioDataBean;
import it.poste.bancoposta.pages.*;
import it.poste.bancoposta.utils.SoftAssertion;
import test.automation.core.UIUtils;
import utils.DinamicData;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="SalvadanaioCreateObjData")
@Android
@IOS
public class SalvadanaioCreateObjDataMan extends LoadableComponent<SalvadanaioCreateObjDataMan> implements SalvadanaioCreateObjData
{

	protected UiPage page;

	@Override
	public void init(UiPage page) {
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) {
		load();
	}

	@Override
	protected void load() {
		page.get();
		
		Properties t=page.getLanguage();
		
		SoftAssertion.get().getAssertion().assertThat(page.getParticle(Locators.SalvadanaioCreateObjDataMolecola.HEADER).getElement().getText())
		.withFailMessage("Controllo header salvadanaio data")
		.isEqualTo(t.getProperty("salvadanaioCreateHeaderImporto"));
		
		SoftAssertion.get().getAssertion().assertThat(page.getParticle(Locators.SalvadanaioCreateObjDataMolecola.DESCRIPTIONDURATA).getElement().getText())
		.withFailMessage("Controllo header salvadanaio data")
		.isEqualTo(t.getProperty("salvadanaioCreateDescrData"));
		
		SoftAssertion.get().getAssertion().assertThat(page.getParticle(Locators.SalvadanaioCreateObjDataMolecola.TITLE).getElement().getText())
		.withFailMessage("Controllo header salvadanaio data")
		.isEqualTo(t.getProperty("salvadanaioCreateTitleData"));
	}

	@Override
	protected void isLoaded() throws Error {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void selectData(List<SalvadanaioDataBean> b) 
	{
		page.getParticle(Locators.SalvadanaioCreateObjDataMolecola.EDITDURATA).getElement().click();
		WaitManager.get().waitShortTime();
		WebElement current=page.getParticle(Locators.SalvadanaioCreateObjDataMolecola.CURRENTDATE).visibilityOfElement(3L);
		SimpleDateFormat f=new SimpleDateFormat("dd MMM yyyy",Locale.ENGLISH);
		
		try {
			Date d=f.parse(current.getAttribute("content-desc").trim());
			f=new SimpleDateFormat("dd/MM/yyyy");
			String date=f.format(d);
			DinamicData.getIstance().set("SALVADANAIO_DATE_SELECTED", date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		page.getParticle(Locators.SalvadanaioCreateObjDataMolecola.DATASELECTOK).getElement().click();
		
		WaitManager.get().waitShortTime();
	}

	@Override
	public void checkDataIsSelected() 
	{
		String selected=page.getParticle(Locators.SalvadanaioCreateObjDataMolecola.DURATAOBIETTIVO).getElement().getText().trim();
		
		org.springframework.util.Assert.isTrue(selected.equals(DinamicData.getIstance().get("SALVADANAIO_DATE_SELECTED")), "data non selezionata");
		
		System.out.println("check data OK");
	}

	@Override
	public void clickAvanti() {
		page.getParticle(Locators.SalvadanaioCreateObjDataMolecola.AVANTIBTN).getElement().click();	
	}
	
}
