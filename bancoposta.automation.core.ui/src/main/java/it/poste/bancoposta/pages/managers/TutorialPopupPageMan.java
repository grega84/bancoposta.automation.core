package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.TutorialPopUpBean;
import io.appium.java_client.AppiumDriver;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.TutorialPopupPage;
import test.automation.core.UIUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="TutorialPopupPage")
@Android
@IOS
public class TutorialPopupPageMan extends LoadableComponent<TutorialPopupPageMan> implements TutorialPopupPage
{
	UiPage page;
	
	private WebElement testo;
	private WebElement header;
	private WebElement description;
	private WebElement apriButton;

	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
		page.get();
		
		
	}

	public void checkTutorialPage(TutorialPopUpBean tutorialPopUpBean) 
	{
		testo=page.getParticle(Locators.TutorialPopupMolecola.TESTO).visibilityOfElement(3L);
		header=page.getParticle(Locators.TutorialPopupMolecola.TUTORIAL_TEXT_HEADER).visibilityOfElement(3L);
		description=page.getParticle(Locators.TutorialPopupMolecola.TUTORIAL_TEXT_DESCRIPTION).visibilityOfElement(3L);
		apriButton=page.getParticle(Locators.TutorialPopupMolecola.TUTORIAL_ACCESS_BUTTON).visibilityOfElement(3L);
		
		System.out.println("tutorial popup");
		System.out.println(testo.getText());
		System.out.println(header.getText());
		System.out.println(description.getText());
		System.out.println(apriButton.getText());
		
		org.springframework.util.Assert.isTrue(testo.getText().trim().equals(tutorialPopUpBean.getTutorial()), "tutorial non conforme alle specifiche actual:"+testo.getText()+", atteso:"+tutorialPopUpBean.getTutorial());
		org.springframework.util.Assert.isTrue(header.getText().trim().equals(tutorialPopUpBean.getHeader()), "tutorial header non conforme alle specifiche actual:"+header.getText()+", atteso:"+tutorialPopUpBean.getHeader());
		org.springframework.util.Assert.isTrue(description.getText().trim().equals(tutorialPopUpBean.getDescription()), "tutorial description non conforme alle specifiche actual:"+description.getText()+", atteso:"+tutorialPopUpBean.getDescription());
		org.springframework.util.Assert.isTrue(apriButton.getText().trim().equals(tutorialPopUpBean.getButton()), "tutorial button non conforme alle specifiche actual:"+apriButton.getText()+", atteso:"+tutorialPopUpBean.getButton());
	}

	public void clickApriButton() 
	{
		apriButton=page.getParticle(Locators.TutorialPopupMolecola.TUTORIAL_ACCESS_BUTTON).visibilityOfElement(3L);
		apriButton.click();
	}

	@Override
	public void init(UiPage page) {
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) {
		load();
	}


}
