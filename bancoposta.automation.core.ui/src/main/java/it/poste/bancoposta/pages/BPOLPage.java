package it.poste.bancoposta.pages;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.pagemanager.AbstractPageManager;
import bean.datatable.CredentialAccessBean;
import bean.datatable.PaymentCreationBean;
import bean.datatable.NotificaPushBean;
import test.automation.core.UIUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public interface BPOLPage extends AbstractPageManager
{

	public void gotoAreaPersonale();

	public void login(CredentialAccessBean b);

	public void gotoBancoPostaOnline();

	public void inviaNotificaPush();

	public void checkIsBancoPostaPage();

	public void gotoContoCarteFromBP();

	public void navigaBersoPostagiro();

	public void compilaPostagiro(PaymentCreationBean p);

	public void checkOperationNotificaOK(NotificaPushBean b);

	public void setDriver(WebDriver bpol);

}
