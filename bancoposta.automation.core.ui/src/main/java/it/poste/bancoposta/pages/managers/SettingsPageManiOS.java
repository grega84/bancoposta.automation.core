package it.poste.bancoposta.pages.managers;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import it.poste.bancoposta.pages.Locators;
@PageManager(page="SettingsPage")
@IOS
public class SettingsPageManiOS extends SettingsPageMan {

	
	private static final String HEADER_TITLE = "Impostazioni";
	private static final String ACCAUTH_TITLE = "Accesso e autorizzazione";
	private static final String POSTEID_TITLE = "Codice PosteID";
	//-----------------ANNUNZIATA---------------------------
	private static final String NOTIFICATION_TITLE = "Notifiche";
	//-----------------ANNUNZIATA---------------------------
	private static final String PRIVACYCOND_TITLE = "Condizioni di servizio e Privacy";
	private static final String VERS_TITLE = "Versione App";
	
	
	private WebElement accessAndAuth;
	private WebElement header;
	private WebElement notification;
	private WebElement privacyAndCondition;
	private WebElement posteIDCode;
	private WebElement appVersion;

	@Override
	protected void load() 
	{
		System.out.println("SETTING PAGE MANAGER IOS");
		page.get();
		this.header=page.getParticle(Locators.SettingsPageMolecola.HEADER).getElement();
		this.accessAndAuth=page.getParticle(Locators.SettingsPageMolecola.ACCESSANDAUTH).getElement();
		this.posteIDCode=page.getParticle(Locators.SettingsPageMolecola.POSTEIDCODE).getElement();
		this.notification=page.getParticle(Locators.SettingsPageMolecola.NOTIFICATION).getElement();
		this.privacyAndCondition=page.getParticle(Locators.SettingsPageMolecola.PRIVACYANDCONDITION).getElement();
		this.appVersion=page.getParticle(Locators.SettingsPageMolecola.APPVERSION).getElement();

		//check sui titoli degli elementi
		Assert.assertTrue("controllo header",this.header.getText().trim().equals(HEADER_TITLE));
		Assert.assertTrue("controllo tasto accesso e autorizzazione",this.accessAndAuth.getText().trim().equals(ACCAUTH_TITLE));
		Assert.assertTrue("controllo tasto posteid",this.posteIDCode.getText().trim().equals(POSTEID_TITLE));
		Assert.assertTrue("controllo tasto notifiche",this.notification.getText().trim().equals(NOTIFICATION_TITLE));
		Assert.assertTrue("controllo tasto privacy",this.privacyAndCondition.getText().trim().equals(PRIVACYCOND_TITLE));
		String t=this.appVersion.getText().trim();
		Assert.assertTrue("controllo tasto appversion",this.appVersion.getText().trim().contains(VERS_TITLE));
	}
}
