package it.poste.bancoposta.pages;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.pagemanager.AbstractPageManager;
import bean.datatable.TutorialPopUpBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;
import test.automation.core.UIUtils;
import test.automation.core.cmd.adb.AdbCommandPrompt;
import test.automation.core.cmd.adb.AdbKeyEvent;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Properties;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public interface IlTuoAndamentoPage extends AbstractPageManager
{

	public void checkEntrateTab();

	 void checkSubPage();

	public void checkUsciteTab();

	public void checkDifferenzaTab();

	public void checkGraphSeekBarEntrate();


	public void checkGraphSeekBarUscite();

	public void checkGraphSeekBarAndamento();

	 void checkSideBar(boolean andamento);

	 void checkGrafoAfterClickOnSidebar(String fm, String ms, String sb1, String sb2);
	
	public void clickSideBar(double d);

	public void clickUsciteTab();

	public void clickEntrateTab();

	public void clickDifferenzaTab();

	public void clickRightMenu();

	public void esportaCsv();

	public void readAndCheckCsv() throws Exception;

	public void removeCsv();

	public void clickOnBack();

}
