package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import automation.core.ui.uiobject.UiPage;
import automation.core.ui.uiobject.UiPageRepository;
import bean.datatable.CredentialAccessBean;
import it.poste.bancoposta.pages.*;
import test.automation.core.UIUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="AccessAuthorizationPage")
@Android
@IOS
public class AccessAuthorizationPageMan extends LoadableComponent<AccessAuthorizationPageMan> implements AccessAuthorizationPage
{

	protected UiPage page;
	private WebElement toggle;
	
	
	@Override
	protected void isLoaded() throws Error {
		page.get();
	}

	@Override
	protected void load() 
	{
//		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.AccessAuthorizationPageMolecola.HEADER).getElement());
//		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.AccessAuthorizationPageLocator.FINGERPRINTHEADER).getElement());
//		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.AccessAuthorizationPageLocator.INFOAPPHEADER).getElement());
//		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.AccessAuthorizationPageLocator.TOGGLE).getElement());

		
	}

	public void toggleFingerPrint(String togleFlag, CredentialAccessBean b) 
	{
		WebElement e=UIUtils.ui().waitForCondition(page.getDriver(), ExpectedConditions.visibilityOfElementLocated(page.getParticle(Locators.AccessAuthorizationPageMolecola.FINGERTOGGLE).getXPath()));

		String status=e.getAttribute("checked");

		if(togleFlag.equals("si") && status.equals("false"))
		{
			e.click();

			WaitManager.get().waitMediumTime();

			InsertPosteIDPage posteId = (InsertPosteIDPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("InsertPosteIDPage", page.getDriver().getClass(), page.getLanguage());
			
			posteId.checkPage();
			posteId.insertPosteID(b.getPosteid());

		}
		else if(togleFlag.equals("no") && status.equals("true"))
		{
			e.click();

			WaitManager.get().waitMediumTime();

			InsertPosteIDPage posteId = (InsertPosteIDPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("InsertPosteIDPage", page.getDriver().getClass(), page.getLanguage());
			
			posteId.checkPage();
			posteId.insertPosteID(b.getPosteid());

		}
	}

	public HomePage gotoHomePage(String userHeader) 
	{
//		WebElement e =page.getParticle(Locators.SettingsPageLocator.LEFTMENU).getElement();
//		e.click();
//		try {Thread.sleep(2000);} catch(Exception err) {}
//		e=page.getParticle(Locators.SettingsPageLocator.LEFTMENU).getElement();
//		e.click();
//		try {Thread.sleep(2000);} catch(Exception err) {}
//		e=page.getParticle(Locators.HomepageHamburgerMenuLocator.HOME).getElement();
//		e.click();
//
//		return new HomePage(driver, driverType, userHeader).get();
		
		SettingsPage settings=(SettingsPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SettingsPage", page.getDriver().getClass(), page.getLanguage());
		
		
		settings.clickLeftButton();
		
		WaitManager.get().waitMediumTime();
		
		settings.clickLeftButton();
		
		WaitManager.get().waitMediumTime();
		
		return settings.openHamburgerMenu().gotoHomePage(userHeader);
	}

	@Override
	public void init(UiPage page) 
	{
		this.page=page;
	}

	@Override
	public void checkPage(Object...params) 
	{
		isLoaded();
	}

	@Override
	public void clickOnToggle() {
		this.toggle = page.getParticle(Locators.AccessAuthorizationPageMolecola.TOGGLE).getElement();
		String t = toggle.getAttribute("checked");
		if (t.equals("false")) {
			toggle.click();

		} else {
			toggle.click();

			WaitManager.get().waitShortTime();
			toggle.click();

		}
	}

}
