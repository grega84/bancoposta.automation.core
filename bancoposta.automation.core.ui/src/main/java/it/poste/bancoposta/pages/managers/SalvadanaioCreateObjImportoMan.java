package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.Entry;
import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.CredentialAccessBean;
import bean.datatable.SalvadanaioDataBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.LocksDevice;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.android.nativekey.PressesKey;
import it.poste.bancoposta.pages.DeviceNative;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.SalvadanaioCreateObjImporto;
import it.poste.bancoposta.utils.SoftAssertion;
import test.automation.core.UIUtils;

import java.util.Properties;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="SalvadanaioCreateObjImporto")
@Android
@IOS
public class SalvadanaioCreateObjImportoMan extends LoadableComponent<SalvadanaioCreateObjImportoMan> implements SalvadanaioCreateObjImporto{

	UiPage page;

	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
		page.get();
		
		Properties t=page.getLanguage();
		
		SoftAssertion.get().getAssertion().assertThat(page.getParticle(Locators.SalvadanaioCreateObjImportoMolecola.HEADER).getElement().getText())
		.withFailMessage("Controllo header salvadanaio importo")
		.isEqualTo(t.getProperty("salvadanaioCreateHeaderImporto"));
	}

	@Override
	public void init(UiPage page) 
	{
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) 
	{
		load();
		
	}

	@Override
	public void insertImporto(SalvadanaioDataBean c) 
	{
		WebElement e=page.getParticle(Locators.SalvadanaioCreateObjImportoMolecola.IMPORTO).visibilityOfElement(10L);
		e.clear();
		e.sendKeys(c.getAmount());
	}

	@Override
	public void clickAvanti() {
		page.getParticle(Locators.SalvadanaioCreateObjImportoMolecola.AVANTIBTN).visibilityOfElement(10L).click();
		WaitManager.get().waitMediumTime();
	}
}
