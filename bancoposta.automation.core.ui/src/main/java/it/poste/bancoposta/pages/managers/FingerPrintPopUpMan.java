package it.poste.bancoposta.pages.managers;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.uiobject.UiPage;
import io.appium.java_client.AppiumDriver;
import it.poste.bancoposta.pages.FingerPrintPopUp;
import it.poste.bancoposta.pages.Locators;
import test.automation.core.UIUtils;

@PageManager(page="FingerPrintPopUp")
@Android
@IOS
public class FingerPrintPopUpMan extends LoadableComponent<FingerPrintPopUpMan> implements FingerPrintPopUp
{
	UiPage page;
	
	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
		page.get();
		
	}

	public void insertFingerPrint(String fingerId) throws Exception 
	{   WaitManager.get().waitMediumTime();
		String exc = "adb -e emu finger touch " + fingerId;
		Runtime.getRuntime().exec(exc);
		System.out.println("invocazione adb finger touch ok "+exc);
	}
	
	public void clickCancelButton()
	{
		page.getParticle(Locators.FingerPrintPopUpMolecola.CANCELBUTTON).visibilityOfElement(10L)
		.click();
	}

	@Override
	public void init(UiPage page) 
	{
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) 
	{
		load();
	}
}