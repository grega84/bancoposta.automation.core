package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Utility;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.BonificoDataBean;
import io.appium.java_client.MobileDriver;
import it.poste.bancoposta.pages.BonificoConfirmSubPage;
import it.poste.bancoposta.pages.Locators;
import test.automation.core.UIUtils;

import java.util.Locale;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="BonificoConfirmSubPage")
@Android
public class BonificoConfirmSubPageMan extends LoadableComponent<BonificoConfirmSubPageMan> implements BonificoConfirmSubPage
{
	 static final String TITLE = "� tutto corretto?";
	 static final String DESCRIPTION = "verifica i dati prima di procedere con il pagamento.";

	UiPage page;

	 WebElement addressSender;
	 WebElement citySender;
	 WebElement sender;
	 WebElement senderEffective;

	 int founds;

	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
		WebDriver driver=page.getDriver();
		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(page.getParticle(Locators.BonificoConfirmPageMolecola.ADDRESSSENDER).getXPath()),60);
		founds++;
		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(page.getParticle(Locators.BonificoConfirmPageMolecola.CITYSENDER).getXPath()));
		founds++;
		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(page.getParticle(Locators.BonificoConfirmPageMolecola.SENDER).getXPath()));
		founds++;
		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(page.getParticle(Locators.BonificoConfirmPageMolecola.SENDEREFFECTIVE).getXPath()));
		founds++;


	}

	public void checkData(BonificoDataBean b) 
	{
		this.addressSender=page.getParticle(Locators.BonificoConfirmPageMolecola.ADDRESSSENDER).getElement();
		this.citySender=page.getParticle(Locators.BonificoConfirmPageMolecola.CITYSENDER).getElement();
		this.sender=page.getParticle(Locators.BonificoConfirmPageMolecola.SENDER).getElement();
		this.senderEffective=page.getParticle(Locators.BonificoConfirmPageMolecola.SENDEREFFECTIVE).getElement();


		//controllo indirizzo
		String txt=this.addressSender.getText().trim();
		String toCheck=b.getAddress();

		Assert.assertTrue("controllo indirizzo;attuale:"+txt+", atteso:"+toCheck,txt.equals(toCheck));

		//controllo localit�
		txt=this.citySender.getText().trim();
		toCheck=b.getCitySender();

		Assert.assertTrue("controllo localit�;attuale:"+txt+", atteso:"+toCheck,txt.equals(toCheck));

		//controllo riferimento beneficiario
		txt=this.sender.getText().trim();
		toCheck=b.getReference();

		Assert.assertTrue("controllo riferimento beneficiario;attuale:"+txt+", atteso:"+toCheck,txt.equals(toCheck));

		//controllo beneficiario effettivo
		txt=this.senderEffective.getText().trim();
		toCheck=b.getRefecenceEffective();

		Assert.assertTrue("controllo beneficiario effettivo;attuale:"+txt+", atteso:"+toCheck,txt.equals(toCheck));
	}

	public boolean ok() 
	{
		return founds == 4;
	}

	@Override
	public void init(UiPage page) 
	{
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) 
	{
		load();
	}
}
