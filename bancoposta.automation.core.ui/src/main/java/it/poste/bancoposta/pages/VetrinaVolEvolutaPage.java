package it.poste.bancoposta.pages;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.w3c.dom.Node;

import automation.core.ui.pagemanager.AbstractPageManager;
import bean.datatable.BiscottoDataBean;
import bean.datatable.TutorialPopUpBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import test.automation.core.UIUtils;
import utils.ObjectFinderLight;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public interface VetrinaVolEvolutaPage extends AbstractPageManager{
;

	public void checkSubPage(BiscottoDataBean b) throws Exception;

	public void clickOnScopriOfferta();

	public VetrinaVolEvolutaPage getLight(String pageSource) throws Exception ;

	public void clickOnTab(String biscotto);

	

}
