package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Utility;
import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.NotificationSettingBean;
import it.poste.bancoposta.pages.BonificoConfirmSubPage;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.SettingsNotificationEdit1Page;
import it.poste.bancoposta.pages.SettingsNotificationMinAmountPage;
import test.automation.core.UIUtils;

import java.util.Locale;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="SettingsNotificationEdit1Page")
@Android
@IOS
public class SettingsNotificationEdit1PageMan extends LoadableComponent<SettingsNotificationEdit1PageMan> implements SettingsNotificationEdit1Page
{
	UiPage page;

	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement header;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement leftMenu;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement switchButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement minAmount;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement editAmount;
	private NotificationSettingBean bean;
	private SettingsNotificationMinAmountPage settingSoglia;

	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
		page.get();
		
		this.header=page.getParticle(Locators.SettingsNotificationEdit1PageMolecola.HEADER).getElement();


		//controllo che l'header sia corretto
		String txt=this.header.getText();
		Assert.assertTrue("controllo header",txt.equals(bean.getOperationType()));
	}

	public void setValue(boolean clickSwitch) throws Exception 
	{
		this.leftMenu=page.getParticle(Locators.SettingsNotificationEdit1PageMolecola.LEFTMENU).getElement();
		this.switchButton=page.getParticle(Locators.SettingsNotificationEdit1PageMolecola.SWITCHBUTTON).getElement();
		

		//controllo che lo switch sia gia selezionato, se no lo clicco qualora clickSwitch sia true
		boolean checked=this.switchButton.getText().equals("ON");

		if(!checked && clickSwitch)
			this.switchButton.click();

		WaitManager.get().waitShortTime();

		this.minAmount=page.getParticle(Locators.SettingsNotificationEdit1PageMolecola.MINAMOUNT).getElement();
		this.editAmount=page.getParticle(Locators.SettingsNotificationEdit1PageMolecola.EDITAMOUNT).getElement();
		

		String val=this.minAmount.getText().trim().replace("\u20AC", "").replace(" ", "");

		Integer currentVal=Integer.parseInt(val);
		//Integer currentVal=(int) Utility.parseCurrency(val, Locale.ITALY);
		Integer toVal=Integer.parseInt(bean.getAmount());

		//controllo che il valore settato in soglia minima sia identico a quello passato in
		//input, se no setto il valore a quello nuovo

		if(currentVal != toVal)
		{
			//setto il valore uguale a amount
			this.editAmount.click();

			settingSoglia=(SettingsNotificationMinAmountPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("SettingsNotificationMinAmountPage", page.getDriver().getClass(), page.getLanguage());
			settingSoglia.checkPage();

			settingSoglia.setSoglia(currentVal,toVal);

		}

		this.leftMenu=page.getParticle(Locators.SettingsNotificationEdit1PageMolecola.LEFTMENU).getElement();
		this.switchButton=page.getParticle(Locators.SettingsNotificationEdit1PageMolecola.SWITCHBUTTON).getElement();
		
		//al termine clicco su back
		this.leftMenu.click();
	}

	public void checkValues(NotificationSettingBean bean) 
	{
		boolean checked=false;
		this.leftMenu=page.getParticle(Locators.SettingsNotificationEdit1PageMolecola.LEFTMENU).getElement();
		this.switchButton=page.getParticle(Locators.SettingsNotificationEdit1PageMolecola.SWITCHBUTTON).getElement();
		this.minAmount=page.getParticle(Locators.SettingsNotificationEdit1PageMolecola.MINAMOUNT).getElement();
		//this.editAmount=page.getParticle(Locators.SettingsNotificationEdit1PageMolecola.EDITAMOUNT.getAndroidLocator()));
		checked=this.switchButton.getText().equals("ON");
		

		//controllo che lo switch sia gia selezionato, se no lo clicco qualora clickSwitch sia true

		boolean toCheck=bean.getValue().equals("si");

		String val=this.minAmount.getText().trim().replace("\u20AC", "").replace(" ", "");

		Integer currentVal=Integer.parseInt(val);
		Integer toVal=Integer.parseInt(bean.getAmount());

		Assert.assertTrue("controllo valore corrente = a valore da settare;attuale="+currentVal+", atteso:"+toVal,currentVal == toVal);
		Assert.assertTrue("controllo switch settato correttamente;",checked == toCheck);

		//al termine clicco su back
		this.leftMenu.click();
	}

	@Override
	public void init(UiPage page) {
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) {
		load();
	}
}
