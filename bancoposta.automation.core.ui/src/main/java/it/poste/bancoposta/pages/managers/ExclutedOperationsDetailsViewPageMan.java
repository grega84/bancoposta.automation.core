package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.uiobject.UiPage;
import it.poste.bancoposta.pages.ExclutedOperationsDetailsViewPage;
import it.poste.bancoposta.pages.Locators;
import test.automation.core.UIUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="ExclutedOperationsDetailsViewPage")
@Android
@IOS
public class ExclutedOperationsDetailsViewPageMan extends LoadableComponent<ExclutedOperationsDetailsViewPageMan> implements ExclutedOperationsDetailsViewPage
{
	UiPage page;

	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement header;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement leftmenu;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement categoryHeader;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement editButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement menuButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement categoryTag;
	/**
	 * @android 
	 * @ios campo dinamico sostituire $(opTitle) con il nome dell'operazione da ricercare
	 * @web 
	 */
	private WebElement operationTitle;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement operationDetails;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement operationAmmount;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement dataValuta;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement dataContabile;

	
	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
		page.get();
	}

	@Override
	public void init(UiPage page) 
	{
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) 
	{
		load();
	}
}
