package it.poste.bancoposta.pages.managers;

import org.junit.Assert;

import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import it.poste.bancoposta.pages.Locators;

@PageManager(page="EnableYourProductApp")
@IOS
public class EnableYourProductAppManIOS extends EnableYourProductAppMan 
{
	public void checkEnableYourProductPage()
	{
		this.enableProductheader =page.getParticle(Locators.EnableYourProductMolecola.ENABLEPRODUCTHEADER).getElement();
		enableProductHeadertwo= enableProductheader.getText().trim();
		System.out.println(enableProductHeadertwo);
		
		Assert.assertTrue("Abilita il tuo prodotto in App! errato -->",enableProductheader.isDisplayed());
		
		
		this.closeButton =page.getParticle(Locators.EnableYourProductMolecola.CLOSEBUTTON).getElement();
		
		Assert.assertTrue("close button non presente",closeButton.isDisplayed());
		
		this.deviceImage =page.getParticle(Locators.EnableYourProductMolecola.DEVICEIMAGE).getElement();
		
		
		
		this.descriptionTitle =page.getParticle(Locators.EnableYourProductMolecola.DESCRIPTIONTITLE).getElement();
		descriptionTitleTwo=descriptionTitle.getText().trim();
		
		System.out.println(descriptionTitleTwo);
		
		Assert.assertTrue("Abilita anche questo dispositivo ad operare! errato",descriptionTitle.isDisplayed());
		
		
		this.startButton =page.getParticle(Locators.EnableYourProductMolecola.STARTBUTTON).getElement();
		
		Assert.assertTrue("inizia non presente",startButton.isDisplayed());
		
		System.out.println("I controlli della pagina sono terminati correttamente!");
	}
}
