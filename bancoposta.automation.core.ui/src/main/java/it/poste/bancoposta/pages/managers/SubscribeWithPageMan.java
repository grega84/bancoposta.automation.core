package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.VoucherVerificationBean;
import it.poste.bancoposta.pages.BonificoConfirmSubPage;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.PayWithPage;
import it.poste.bancoposta.pages.SubscribeWithPage;
import it.poste.bancoposta.pages.VoucherSelectPage;
import test.automation.core.UIUtils;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="SubscribeWithPage")
@Android
@IOS
public class SubscribeWithPageMan extends LoadableComponent<SubscribeWithPageMan> implements SubscribeWithPage
{
	UiPage page;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement header;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement cancelButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement cardElement;
	/**
	 * @android 
	 * @ios campo dinamico sostituire $(index) con l'indice dell'elemento da ricercare
	 * @web 
	 */
	private WebElement cardElementType;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement cardAmount;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement cardNumber;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement messageHeader;

	private VoucherSelectPage voucherPage;

	@Override
	protected void isLoaded() throws Error {
	}

	@Override
	protected void load() 
	{

		WaitManager.get().waitMediumTime();
		page.get();
	}

	public void createBuonoPostale(VoucherVerificationBean b, boolean annulla) 
	{
		String cardLocators="";
		String cardName="";
		String cardNumber="";

		cardLocators=page.getParticle(Locators.SubscribeWithPageMolecola.CARDELEMENT).getLocator();
		cardName=page.getParticle(Locators.SubscribeWithPageMolecola.CARDELEMENTTYPE).getLocator();
		cardNumber=page.getParticle(Locators.SubscribeWithPageMolecola.CARDNUMBER).getLocator();
		
		try 
		{
			PayWithPage payWithPage;
			payWithPage=(PayWithPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("PayWithPage", page.getDriver().getClass(), page.getLanguage());
			payWithPage.checkPage();
			
			payWithPage.clickOnConto(b.getPayWith());
		}catch (Exception err) {}

		//			List<WebElement> cards=driver.findElements(By.xpath(cardLocators));
		//			
		//			//ricerco il prodotto desiderato
		//			for(WebElement card : cards)
		//			{
		//
		//				String name=card.findElement(By.xpath(cardName)).getText().trim().toLowerCase();
		//				String number=card.findElement(By.xpath(cardNumber)).getText().trim().toLowerCase();
		//				
		//				String[] cardInfo=b.getPayWith().split(";");
		//				
		//				//ok trovato quello che ci serve quindi clicco
		//				if(name.equals(cardInfo[0]) && number.equals(cardInfo[1]))
		//				{
		//					card.click();
		//					break;
		//				}
		//			}

		voucherPage=(VoucherSelectPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("VoucherSelectPage", page.getDriver().getClass(), page.getLanguage());
		
		voucherPage.checkPage();
		//procedo con la sottoscrizione del buono qualore annulla=false
		voucherPage.subScribeBuono(b,annulla);
	}

	@Override
	public void init(UiPage page) {
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) {
		load();
	}
}
