package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.w3c.dom.NodeList;

import android.Utility;
import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.TransactionDetailBean;
import io.appium.java_client.AppiumDriver;
import it.poste.bancoposta.pages.BonificoConfirmSubPage;
import it.poste.bancoposta.pages.ExclutedOperationsPage;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.YourExpencesDetailsPage;
import it.poste.bancoposta.pages.YourExpencesPage;
import test.automation.core.UIUtils;
import test.automation.core.cmd.adb.AdbCommandPrompt;
import test.automation.core.cmd.adb.AdbKeyEvent;
import utils.Entry;
import utils.ObjectFinderLight;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Properties;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="YourExpencesDetailsPage")
@Android
public class YourExpencesDetailsPageMan extends LoadableComponent<YourExpencesDetailsPageMan> implements YourExpencesDetailsPage
{
	UiPage page;

	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement header;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement leftmenu;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement rightMenu;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement monthList;
	/**
	 * @android campo dinamico sostituire $(month) con il nome del mese da ricercare
	 * @ios campo dinamico sostituire $(month) con il mese da ricercare
	 * @web 
	 */
	private WebElement monthElement;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement pieChart;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement detailsList;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement detailsElement;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement seeExcludedMovements;

	protected static String[] mesi=new String[] {
			"Gennaio",
			"Febbraio",
			"Marzo",
			"Aprile",
			"Maggio",
			"Giugno",
			"Luglio",
			"Agosto",
			"Settembre",
			"Ottobre",
			"Novembre",
			"Dicembre"
	};

	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
		page.get();
	}

	public ExclutedOperationsPage gotoCategory(TransactionDetailBean b) 
	{
		String listXpath="";
		String title="";
		WaitManager.get().waitLongTime();
		WaitManager.get().waitLongTime();

		listXpath=page.getParticle(Locators.YourExpencesDetailsPageMolecola.DETAILSELEMENT).getLocator();
		title=page.getParticle(Locators.YourExpencesDetailsPageMolecola.DETAILSTITLE).getLocator();
		

		List<WebElement> list=page.getDriver().findElements(By.xpath(listXpath));

		//for(WebElement e : list)
		for(int i=0;i<list.size();i++)
		{

			String elementTitle=null;

			elementTitle=list.get(i).findElement(By.xpath(title)).getText().trim().toLowerCase();
			

			if(elementTitle.equals(b.getCategoryTransation().toLowerCase()))
			{
				list.get(i).click();
				break;
			}
		}

		ExclutedOperationsPage p=(ExclutedOperationsPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("ExclutedOperationsPage", page.getDriver().getClass(), page.getLanguage());

		p.setBean(b);
		p.checkPage();

		return p;
	}

	public YourExpencesPage clickBack() 
	{
		this.leftmenu=page.getParticle(Locators.YourExpencesDetailsPageMolecola.LEFTMENU).getElement();
		
		this.leftmenu.click();
		
		YourExpencesPage p=(YourExpencesPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("YourExpencesPage", page.getDriver().getClass(), page.getLanguage());
		
		p.checkPage();

		return p;
	}

	public List<String[]> readSpeseDettaglio() 
	{
		WaitManager.get().waitLongTime();
		String pageSource=page.getDriver().getPageSource();
		NodeList items=ObjectFinderLight.getElements(pageSource, page.getParticle(Locators.YourExpencesDetailsPageMolecola.DETAILSTITLE).getLocator());
		System.out.println(items);
		NodeList values=ObjectFinderLight.getElements(pageSource, page.getParticle(Locators.YourExpencesDetailsPageMolecola.DETAILS_VALUE).getLocator());
		NodeList percentances=ObjectFinderLight.getElements(pageSource, page.getParticle(Locators.YourExpencesDetailsPageMolecola.DETAILS_PERCENTANCE).getLocator());

		org.springframework.util.Assert.isTrue(items.getLength() == values.getLength() && items.getLength() == percentances.getLength(), "spese dettaglio valori non della stessa lunghezza");


		ArrayList<String[]> L=new ArrayList<>();

		for(int i=0;i<items.getLength();i++)
		{
			String[] v=new String[3];
			v[0]=items.item(i).getAttributes().getNamedItem("text").getNodeValue().trim();
			v[1]=values.item(i).getAttributes().getNamedItem("text").getNodeValue().trim().replace(",", ".");
			v[2]=percentances.item(i).getAttributes().getNamedItem("text").getNodeValue().trim();

			L.add(v);

			System.out.println(Arrays.toString(v));
		}

		return L;
	}

	public void clickRightMenu() 
	{
		page.getParticle(Locators.YourExpencesDetailsPageMolecola.RIGHTMENU).visibilityOfElement(10L);
		page.getParticle(Locators.YourExpencesDetailsPageMolecola.RIGHTMENU).getElement().click();
		
		
	}

	public void esportaCSV() 
	{
		page.getParticle(Locators.YourExpencesDetailsPageMolecola.ESPORTA_CSV).visibilityOfElement(10L);
		page.getParticle(Locators.YourExpencesDetailsPageMolecola.ESPORTA_CSV).getElement().click();
		
		WaitManager.get().waitMediumTime();
		
		try {
			page.getParticle(Locators.NewLoginPreLoginPageMolecola.ALLOWPOPUPBUTTON).visibilityOfElement(10L);
			page.getParticle(Locators.NewLoginPreLoginPageMolecola.ALLOWPOPUPBUTTON).getElement().click();
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		WaitManager.get().waitMediumTime();
		
		
		Properties p=UIUtils.ui().getCurrentProperties();
		
		try {
			AdbCommandPrompt c=new AdbCommandPrompt(p.getProperty("adb.path"), p.getProperty("ios.udid"));
			
			c.keyEvent(AdbKeyEvent.KEYCODE_BACK);
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		int m=Calendar.getInstance().get(Calendar.MONTH);
		int y=Calendar.getInstance().get(Calendar.YEAR);
		
		String csvFile="/storage/emulated/0/Download/Uscite_"+mesi[m]+"_"+y+".csv";
		System.out.println(csvFile);
		
		AdbCommandPrompt c=new AdbCommandPrompt(p.getProperty("adb.path"), p.getProperty("ios.udid"));
		
		c.pull(csvFile, "Uscite_"+mesi[m]+"_"+y+".csv");
	}

	public void readandCheckCsv(List<String[]> speseDettaglioList) throws Exception 
	{
		int m=Calendar.getInstance().get(Calendar.MONTH);
		int y=Calendar.getInstance().get(Calendar.YEAR);
		
		Reader reader =new BufferedReader(new FileReader("Uscite_"+mesi[m]+"_"+y+".csv"));
//		Reader reader = Files.newBufferedReader(Paths.get("Uscite_"+mesi[m]+"_"+y+".csv"));
        CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT.withFirstRecordAsHeader());
        
        for(CSVRecord csvRecord : csvParser)
        {
        	String category=csvRecord.get(0).trim();
        	String value=csvRecord.get(1).trim();
        	
        	//boolean find=false;
        	
        	System.out.println("check row:"+category+","+value);
        	
        	for(String[] el : speseDettaglioList)
        	{
        		if(category.equals(el[0]))
        		{
        			//find=true;
        			org.springframework.util.Assert.isTrue(el[1].contains(value), Arrays.toString(el)+" --> KO");
        		}
        	}
        	
        }
        
        System.out.println("csv check [OK]");
	}

	public void removeCsv() 
	{
		try {
			int m=Calendar.getInstance().get(Calendar.MONTH);
			int y=Calendar.getInstance().get(Calendar.YEAR);
			
			String csvFile="/storage/emulated/0/Download/Uscite_"+mesi[m]+"_"+y+".csv";
			System.out.println("delete "+csvFile);
			Properties p=UIUtils.ui().getCurrentProperties();
			
			AdbCommandPrompt c=new AdbCommandPrompt(p.getProperty("adb.path"), p.getProperty("ios.udid"));
			
			c.remove(csvFile);
			
			File f=new File("Uscite_"+mesi[m]+"_"+y+".csv");
			
			f.delete();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void clickMovimentiEsclusi() 
	{
		page.getParticle(Locators.YourExpencesDetailsPageMolecola.SEEEXCLUDEDMOVEMENTS).visibilityOfElement(10L);
		page.getParticle(Locators.YourExpencesDetailsPageMolecola.SEEEXCLUDEDMOVEMENTS).getElement().click();
		
	}

	@Override
	public void init(UiPage page) {
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) {
		load();
	}

}
