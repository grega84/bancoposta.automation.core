package it.poste.bancoposta.pages.managers;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.uiobject.UiPage;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.RicaricaTelefonicaSelezionaOperatore;
import test.automation.core.UIUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="RicaricaTelefonicaSelezionaOperatore")
@Android
@IOS
public class RicaricaTelefonicaSelezionaOperatoreMan extends LoadableComponent<RicaricaTelefonicaSelezionaOperatoreMan> implements RicaricaTelefonicaSelezionaOperatore
{
	UiPage page;


	@Override
	protected void load() 
	{
		page.get();
	}
	@Override
	protected void isLoaded() throws Error {
		
	}
	@Override
	public void init(UiPage page) {
		this.page=page;
	}
	@Override
	public void checkPage(Object... params) {
		load();
	}




}
