package it.poste.bancoposta.pages.managers;

import automation.core.ui.Entry;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import bean.datatable.BiscottoDataBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.CommandExecutionHelper;
import io.appium.java_client.ExecutesMethod;
import it.poste.bancoposta.pages.Locators;
import static io.appium.java_client.MobileCommand.setSettingsCommand;

@PageManager(page="VolProductDetailsPage")
@IOS
public class VolProductDetailsPageManIOS extends VolProductDetailsPageMan {
	
	private void restoreDefaultSettings() {
		CommandExecutionHelper.execute((ExecutesMethod) page.getDriver(), setSettingsCommand("customSnapshotTimeout",15));
		CommandExecutionHelper.execute((ExecutesMethod) page.getDriver(), setSettingsCommand("snapshotMaxDepth",50));
		CommandExecutionHelper.execute((ExecutesMethod) page.getDriver(), setSettingsCommand("useFirstMatch",false));
	}

	private void setCustomSettings() {
		CommandExecutionHelper.execute((ExecutesMethod) page.getDriver(), setSettingsCommand("customSnapshotTimeout",0));
		CommandExecutionHelper.execute((ExecutesMethod) page.getDriver(), setSettingsCommand("snapshotMaxDepth",50));
		CommandExecutionHelper.execute((ExecutesMethod) page.getDriver(), setSettingsCommand("useFirstMatch",true));
	}
	
	public void checkFID(BiscottoDataBean b) 
	{
//		page.readPageSource();
//		System.out.println(page.getPageSource());
		setCustomSettings();
		page.getParticle(Locators.VolProductDetailsMolecola.FID_DOC).visibilityOfElement(10L,new Entry("txt", b.getFidCode()));
		restoreDefaultSettings();
		
	}

}
