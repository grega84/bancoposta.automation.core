package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.CredentialAccessBean;
import bean.datatable.SalvadanaioDataBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.android.AndroidDriver;
import it.poste.bancoposta.pages.EnableYourProductApp;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.utils.PageMolecolaImgCreator;
import test.automation.core.UIUtils;

import java.sql.Date;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="EnableYourProductApp")
@Android
public class EnableYourProductAppMan extends LoadableComponent<EnableYourProductAppMan> implements EnableYourProductApp
{

	UiPage page;

	/**
	 * @android 
	 * @ios 
	 * @web  
	 */
	protected WebElement enableProductheader;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */ 
	
	protected WebElement closeButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	
	protected WebElement deviceImage;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	
	protected WebElement descriptionTitle;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	
	protected WebElement description;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	
	protected WebElement discoverLink;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	
	protected WebElement startButton;
	protected String enableProductHeadertwo;
	protected String descriptionTitleTwo;
	protected String descriptionTwo;
	protected String discoverLinkTwo;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	
	@Override
	protected void isLoaded() throws Error {
		
	}  

	@Override
	protected void load() 
	{
		page.get();
		
		

	}
	
	public void checkEnableYourProductPage(){
		
		this.enableProductheader =page.getParticle(Locators.EnableYourProductMolecola.ENABLEPRODUCTHEADER).getElement();
		enableProductHeadertwo= enableProductheader.getText().trim();
		System.out.println(enableProductHeadertwo);
		
		Assert.assertTrue("Abilita il tuo prodotto in App! errato -->"+enableProductHeadertwo,enableProductHeadertwo.equals("Abilita il tuo prodotto in App!"));
		
		
		this.closeButton =page.getParticle(Locators.EnableYourProductMolecola.CLOSEBUTTON).getElement();
		
		Assert.assertTrue("close button non presente",closeButton.isDisplayed());
		
		this.deviceImage =page.getParticle(Locators.EnableYourProductMolecola.DEVICEIMAGE).getElement();
		
		Assert.assertTrue("device image non presente",deviceImage.isDisplayed());
		
		try {
			this.descriptionTitle =page.getParticle(Locators.EnableYourProductMolecola.DESCRIPTIONTITLE).getElement();
			descriptionTitleTwo=descriptionTitle.getText().trim();
			
			System.out.println(descriptionTitleTwo);
			
			Assert.assertTrue("Abilita anche questo dispositivo ad operare! errato -->"+descriptionTitleTwo,descriptionTitleTwo.equals("Abilita anche questo dispositivo ad operare!"));
			
			this.description =page.getParticle(Locators.EnableYourProductMolecola.DESCRIPTION).getElement();
			descriptionTwo=description.getText().trim();
			
			Assert.assertTrue("Descrizione errata -->"+descriptionTwo,descriptionTwo.equals("Poste Italiane sta adottando nuovi strumenti di sicurezza, in conformit� a quanto previsto dalle disposizioni "
					+ "della direttiva europea 2015/2366/UE sui servizi di pagamento, nota come PSD2. Per accedere a tutte le funzionalit� informative e dispositive, devi "
					+ "abilitare anche questo dispositivo ad operare."));
			
			this.discoverLink =page.getParticle(Locators.EnableYourProductMolecola.DISCOVERLINK).getElement();
			
			Assert.assertTrue("scopri di piu non presente",discoverLink.isDisplayed());
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		this.startButton =page.getParticle(Locators.EnableYourProductMolecola.STARTBUTTON).getElement();
		
		Assert.assertTrue("inizia non presente",startButton.isDisplayed());
		
		System.out.println("I controlli della pagina sono terminati correttamente!");
		
	}

	@Override
	public void init(UiPage page) 
	{
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) 
	{
		load();
	}
		
}
