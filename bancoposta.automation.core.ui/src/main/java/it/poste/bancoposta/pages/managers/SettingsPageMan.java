package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import automation.core.ui.uiobject.UiPage;
import it.poste.bancoposta.pages.BonificoConfirmSubPage;
import it.poste.bancoposta.pages.HomepageHamburgerMenu;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.SettingsNotificationPage;
import it.poste.bancoposta.pages.SettingsPage;
import it.poste.bancoposta.utils.Parser;
import test.automation.core.UIUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="SettingsPage")
@Android
public class SettingsPageMan extends LoadableComponent<SettingsPageMan> implements SettingsPage
{
	private static final String HEADER_TITLE = "Impostazioni";
	private static final String ACCAUTH_TITLE = "Accesso e autorizzazione";
	private static final String POSTEID_TITLE = "Codice PosteID";
	//-----------------ANNUNZIATA---------------------------
	private static final String NOTIFICATION_TITLE = "Notifiche";
	//-----------------ANNUNZIATA---------------------------
	private static final String PRIVACYCOND_TITLE = "Condizioni di servizio e Privacy";
	private static final String VERS_TITLE = "Versione app";

	UiPage page;

	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement header;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement leftMenu;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement accessAndAuth;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement posteIDCode;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement notification;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement privacyAndCondition;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement appVersion;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement disableProductButton;
	private WebElement accessAut;

	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
		page.get();

		this.header=page.getParticle(Locators.SettingsPageMolecola.HEADER).getElement();
		this.accessAndAuth=page.getParticle(Locators.SettingsPageMolecola.ACCESSANDAUTH).getElement();
		this.posteIDCode=page.getParticle(Locators.SettingsPageMolecola.POSTEIDCODE).getElement();
		this.notification=page.getParticle(Locators.SettingsPageMolecola.NOTIFICATION).getElement();
		this.privacyAndCondition=page.getParticle(Locators.SettingsPageMolecola.PRIVACYANDCONDITION).getElement();
		this.appVersion=page.getParticle(Locators.SettingsPageMolecola.APPVERSION).getElement();

		//check sui titoli degli elementi
		Assert.assertTrue("controllo header",this.header.getText().trim().equals(HEADER_TITLE));
		Assert.assertTrue("controllo tasto accesso e autorizzazione",this.accessAndAuth.findElement(By.className("android.widget.TextView")).getText().trim().equals(ACCAUTH_TITLE));
		Assert.assertTrue("controllo tasto posteid",this.posteIDCode.findElement(By.className("android.widget.TextView")).getText().trim().equals(POSTEID_TITLE));
		Assert.assertTrue("controllo tasto notifiche",this.notification.findElement(By.className("android.widget.TextView")).getText().trim().equals(NOTIFICATION_TITLE));
		Assert.assertTrue("controllo tasto privacy",this.privacyAndCondition.findElement(By.className("android.widget.TextView")).getText().trim().equals(PRIVACYCOND_TITLE));
		String t=this.appVersion.findElement(By.className("android.widget.TextView")).getText().trim();

		Assert.assertTrue("controllo tasto appversion",this.appVersion.findElement(By.className("android.widget.TextView")).getText().trim().contains(VERS_TITLE));



	}

	public SettingsNotificationPage gotoNotification() 
	{
		this.notification=page.getParticle(Locators.SettingsPageMolecola.NOTIFICATION).getElement();
		
		this.notification.click();
		Parser.parsePageSourceToUix(page.getDriver().getPageSource(), "domRicc");

		SettingsNotificationPage n=(SettingsNotificationPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SettingsNotificationPage", page.getDriver().getClass(), page.getLanguage());

		n.checkPage();

		return n;
	}

	public HomepageHamburgerMenu openHamburgerMenu() 
	{
		this.leftMenu=page.getParticle(Locators.SettingsPageMolecola.LEFTMENU).getElement();
		

		this.leftMenu.click();

		HomepageHamburgerMenu h=(HomepageHamburgerMenu) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("HomepageHamburgerMenu", page.getDriver().getClass(), page.getLanguage());

		h.checkPage();

		return h;
	}

	@Override
	public void init(UiPage page) {
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) {
		load();
	}

	@Override
	public void clickLeftButton() {
		page.getParticle(Locators.SettingsPageMolecola.LEFTMENU).visibilityOfElement(10L).click();
	}

	@Override
	public void clickSettingAuth() {
		this.accessAut = page.getParticle(Locators.SettingsPageMolecola.SETTINGSAUTH).getElement();
		accessAut.click();
		WaitManager.get().waitShortTime();
	}
}
