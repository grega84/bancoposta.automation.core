package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import io.appium.java_client.HidesKeyboard;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Utility;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.VoucherVerificationBean;
import io.appium.java_client.AppiumDriver;
import it.poste.bancoposta.pages.BonificoConfirmSubPage;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.VoucherSelectAmoutPage;
import it.poste.bancoposta.pages.VoucherSubscribePage;
import test.automation.core.UIUtils;

import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="VoucherSelectAmoutPage")
@Android
@IOS
public class VoucherSelectAmoutPageMan extends LoadableComponent<VoucherSelectAmoutPageMan> implements VoucherSelectAmoutPage
{
	UiPage page;

	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement header;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement cancelButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement modifyButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement cardInfo;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement title;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement amount;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement decreaseButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement increaseButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement description;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement availableBalanceTxt;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement confirmButton;
	private VoucherVerificationBean bean;

	private VoucherSubscribePage subscribe;

	
	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
		page.get();
		
		this.cardInfo=page.getParticle(Locators.VoucherSelectAmoutPageMolecola.CARDINFO).getElement();

		String txt=cardInfo.getText().toLowerCase();

		String[] cardInfo=bean.getPayWith().split(";");

		Assert.assertTrue("controllo info carta",txt.equals(cardInfo[0]+" "+cardInfo[1]));


	}

	public void selectAmount(boolean annulla,String posteId) 
	{
		this.cardInfo=page.getParticle(Locators.VoucherSelectAmoutPageMolecola.CARDINFO).getElement();
		this.decreaseButton=page.getParticle(Locators.VoucherSelectAmoutPageMolecola.DECREASEBUTTON).getElement();
		this.increaseButton=page.getParticle(Locators.VoucherSelectAmoutPageMolecola.INCREASEBUTTON).getElement();
		this.amount=page.getParticle(Locators.VoucherSelectAmoutPageMolecola.AMOUNT).getElement();
		this.availableBalanceTxt=page.getParticle(Locators.VoucherSelectAmoutPageMolecola.AVAILABLEBALANCETXT).getElement();
		this.confirmButton=page.getParticle(Locators.VoucherSelectAmoutPageMolecola.CONFIRMBUTTON).getElement();
		this.cancelButton=page.getParticle(Locators.VoucherSelectAmoutPageMolecola.CANCELBUTTON).getElement();
		

		//controllo che il saldo attuale sia maggiore di 50�
		String available=this.availableBalanceTxt.getText().replace("\u20AC", "");
		Pattern pattern = Pattern.compile("[0-9]+,[0-9]+");
		Matcher matcher = pattern.matcher(available);

		matcher.find();

		available=matcher.group();

		available=available.replace(",", ".").trim();

		double currentAmount=Double.parseDouble(available);

		Assert.assertTrue("controllo che il saldo attuale sia maggiore di 50\u20AC saldo attuale:"+currentAmount,currentAmount >= 50.00);


		//controllo che l'editbox sia editabile e che l'importo max inseribile non superi
		//il saldo attuale

		this.amount.clear();
		this.amount.sendKeys("99999999");

		try
		{
			((HidesKeyboard) page.getDriver()).hideKeyboard();
		}
		catch(Exception err)
		{

		}

		String modAmount=this.amount.getText().replace("\u20AC", "").trim();
		//modAmount=modAmount.substring(0, modAmount.indexOf(",")).replace(".", "");
		//int newAmount=(int) Double.parseDouble(modAmount);
		int newAmount=(int) Utility.parseCurrency(modAmount, Locale.ITALY);
		int currentAm=(int) currentAmount;

		//Assert.assertTrue("controllo massimale creazione buono uguale a saldo contabile newAmount:"+newAmount+", currentAmount:"+currentAmount,newAmount == currentAm);

		this.amount.clear();
		this.amount.sendKeys("0");

		try
		{
			((HidesKeyboard) page.getDriver()).hideKeyboard();
		}
		catch(Exception err)
		{

		}

		//setto il valore del buono a quello desiderato
		double toAmount=Double.parseDouble(bean.getAmount());

		int numOfClicks=(int) (toAmount / 50);

		for(int i=0;i<numOfClicks;i++)
		{
			this.increaseButton.click();
			try {
				Thread.sleep(300);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		//controllo che il valore mostrato sia corretto
		modAmount=this.amount.getText().replace("\u20AC", "").trim();
		modAmount=modAmount.substring(0, modAmount.indexOf(",")).replace(".", "");
		double mod=Double.parseDouble(modAmount);

		Assert.assertTrue("controllo che il valore mostrato sia corretto; valore settato:"+mod+", valore desiderato:"+toAmount,mod == toAmount);

		//clicco su conferma
		this.confirmButton.click();

		subscribe= (VoucherSubscribePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("VoucherSubscribePage", page.getDriver().getClass(), page.getLanguage());

		subscribe.checkPage();

		if(!annulla)
		{
			subscribe.subscribe(posteId);
		}
		else
		{
			subscribe.back();
			this.cancelButton.click();
		}

	}

	@Override
	public void init(UiPage page) {
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) {
		load();
	}
}
