package it.poste.bancoposta.pages;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.pagemanager.AbstractPageManager;
import bean.datatable.CredentialAccessBean;
import bean.datatable.QuickOperationCreationBean;
import bean.datatable.SalvadanaioDataBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.android.AndroidDriver;
import test.automation.core.UIUtils;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public interface SalvadanaioConfirmPage extends AbstractPageManager {

	public void checkData(SalvadanaioDataBean bean);
	public void checkData2(QuickOperationCreationBean bean);


	public void checkData3(SalvadanaioDataBean bean);



	public void checkData4(SalvadanaioDataBean bean);


	public void clicksOnButtons2();


	public void checkData5(SalvadanaioDataBean bean);

	public void clickConferma();

}
