package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.CredentialAccessBean;
import io.appium.java_client.android.AndroidDriver;
import it.poste.bancoposta.pages.BonificoConfirmSubPage;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.ObjectiveCreatedPage;
import test.automation.core.UIUtils;

import java.sql.Date;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="ObjectiveCreatedPage")
@Android
@IOS
public class ObjectiveCreatedPageMan extends LoadableComponent<ObjectiveCreatedPageMan> implements ObjectiveCreatedPage
{

	UiPage page;

	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement thankYouPageIcon;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement objectiveCreatedTitle;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	private WebElement objectiveDescription;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */ 

	private WebElement summaryTargetTitle;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	private WebElement payButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	private WebElement notNowButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */


	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override 
	protected void load() 
	{
		page.get();
	}

	public void closeTyp(){

		ObjectiveCreatedPage o = (ObjectiveCreatedPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("ObjectiveCreatedPage", page.getDriver().getClass(), page.getLanguage());
		o.checkPage();

		objectiveCreatedTitle = page.getParticle(Locators.ObjectiveCreatedMolecola.OBJECTIVECREATEDTITLE).getElement();
		org.springframework.util.Assert.isTrue(objectiveCreatedTitle.getText().trim().equals("Obiettivo creato!")); 

		notNowButton =page.getParticle(Locators.ObjectiveCreatedMolecola.NOTNOWBUTTON).getElement() ;
		notNowButton.click();




	}

	@Override
	public void init(UiPage page) {
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) {
		load();
	}

}
