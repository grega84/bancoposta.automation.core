package it.poste.bancoposta.pages;

import automation.core.ui.pagemanager.AbstractPageManager;


public interface NewLoginPreLoginPage extends AbstractPageManager{

	public void makeAnewLogin();

	public void makeAnewLoginAccess();

	public void clickScopriProdotti();

	public void clickOnAccediPrelogin();

	public void clickOnOkPopup();

	public void closeNotifichePopup();

	public void closePersonalizzaEsperienzaInApp();
	
	public void closeFingerPrintPopup();

}
