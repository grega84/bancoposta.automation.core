package it.poste.bancoposta.pages.managers;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Utility;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.BolloAutoBean;
import bean.datatable.BonificoDataBean;
import bean.datatable.QuickOperationCreationBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.android.AndroidDriver;
import it.poste.bancoposta.pages.BolloAutoConfirmPage;
import it.poste.bancoposta.pages.BolloAutoRiepilogoPage;
import it.poste.bancoposta.pages.InsertPosteIDPage;
import it.poste.bancoposta.pages.Locators;
import test.automation.core.UIUtils;

import java.util.Locale;

import org.apache.poi.xwpf.usermodel.TOC;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
@PageManager(page="BolloAutoRiepilogoPage")
@Android
@IOS
public class BolloAutoRiepilogoPageMan extends LoadableComponent<BolloAutoRiepilogoPageMan> implements BolloAutoRiepilogoPage 
{
	UiPage page;

	private WebElement header;
	private WebElement backbutton;
	private WebElement assisstenzaButton;
	private WebElement commissione;
	private WebElement Importo;
	private WebElement regione;
	private WebElement targa;
	private WebElement pagaCon;
	private WebElement confermaButton;

	@Override
	protected void isLoaded() throws Error {

	}

	@Override
	protected void load() {
		page.get();

	}

	public InsertPosteIDPage checkData(BolloAutoBean b) {

		BolloAutoConfirmPage c = (BolloAutoConfirmPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("BolloAutoConfirmPage", page.getDriver().getClass(), page.getLanguage());

		this.header = page.getParticle(Locators.BolloAutoRiepilogoPageMolecola.HEADER).getElement();
		//		    this.backbutton=driver.findElement(By.xpath(Locators.BolloAutoRiepilogoPageMolecola.BACKBUTTON.getAndroidLocator()));			 
		//			this.assisstenzaButton=driver.findElement(By.xpath(Locators.BolloAutoRiepilogoPageMolecola.COMMISSIONE.getAndroidLocator()));
		this.commissione = page.getParticle(Locators.BolloAutoRiepilogoPageMolecola.COMMISSIONE).getElement();
		this.Importo = page.getParticle(Locators.BolloAutoRiepilogoPageMolecola.IMPORTO).getElement();
		this.regione = page.getParticle(Locators.BolloAutoRiepilogoPageMolecola.REGIONE).getElement();
		this.targa = page.getParticle(Locators.BolloAutoRiepilogoPageMolecola.TARGA).getElement();
		this.pagaCon = page.getParticle(Locators.BolloAutoRiepilogoPageMolecola.PAGACON).getElement();
		this.confermaButton = page.getParticle(Locators.BolloAutoRiepilogoPageMolecola.CONFERMABUTTON).getElement();


		Assert.assertTrue(header.getText().trim().equals("Riepilogo"));
		System.out.println("L'header � giusto");

		String comm = commissione.getText().replace("�", "").replace(",", ".").trim();
		Assert.assertTrue(comm.equals(b.getCommissione().trim().toLowerCase()));
		System.out.println("La commissione � giusta");

		BolloAutoConfirmPage bollo = (BolloAutoConfirmPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("BolloAutoConfirmPage", page.getDriver().getClass(), page.getLanguage());
		//    bollo.getTotaleImporto().getText();
		//		Assert.assertTrue(importo.isDisplayed());
		//		System.out.println("L'importo � visualizzato");
		Assert.assertTrue(targa.getText().trim().toLowerCase().equals(b.getLicensePlate().trim().toLowerCase()));
		System.out.println("La targa � giusta");
		System.out.println(pagaCon.getText());
		Assert.assertTrue(pagaCon.getText().trim().toLowerCase().equals(b.getPayWith2().trim().toLowerCase()));
		System.out.println("La carta visualizzata � giusta");


		this.confermaButton.click();

		InsertPosteIDPage p = (InsertPosteIDPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("InsertPosteIDPage", page.getDriver().getClass(), page.getLanguage());
		p.checkPage();
		
		return p;
	}

	@Override
	public void init(UiPage page) 
	{
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) 
	{
		load();
	}

}
