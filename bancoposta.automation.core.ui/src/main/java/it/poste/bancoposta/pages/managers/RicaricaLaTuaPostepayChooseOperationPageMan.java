package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.uiobject.UiPage;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.RicaricaLaTuaPostepayChooseOperationPage;
import test.automation.core.UIUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="RicaricaLaTuaPostepayChooseOperationPage")
@Android
@IOS
public class RicaricaLaTuaPostepayChooseOperationPageMan extends LoadableComponent<RicaricaLaTuaPostepayChooseOperationPageMan> implements RicaricaLaTuaPostepayChooseOperationPage
{
	UiPage page;

	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement header;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement cancellButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement headerImage;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement title;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement titleDescription;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement iniziaButton;

	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	private WebElement continuaButton;

	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
		page.get();
	}

	public RicaricaLaTuaPostepayChooseOperationPage gotoIniziaAbilitaLibrettoInApp() 
	{
		this.iniziaButton=page.getParticle(Locators.RicaricaLaTuaPostepayChooseOperationPageMolecolas.INIZIABUTTON).getElement();
		this.iniziaButton.click();


		return this;
	}

	@Override
	public void init(UiPage page) 
	{
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) {
		load();
	}
}
