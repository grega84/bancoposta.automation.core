package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Utility;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import automation.core.ui.uiobject.UiPage;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileDriver;
import it.poste.bancoposta.pages.BonificoConfirmSubPage;
import it.poste.bancoposta.pages.FAQPage;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.NumeriUtiliPage;
import it.poste.bancoposta.pages.PosteChatPage;
import test.automation.core.UIUtils;
import test.automation.core.UIUtils.SCROLL_DIRECTION;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="FAQPage")
@Android
@IOS
public class FAQPageMan extends LoadableComponent<FAQPageMan> implements FAQPage
{
	private static final String HELP_TEXT = "Hai bisogno di aiuto?";
	private static final String HEADER = "Assistenza";
	
	UiPage page;
	
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement header;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement cancellButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement helpSubSection;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement contactButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement chatWithUsButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement appUfficioPostaleLink;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement appPostePayLink;
	private WebElement contactUsButton;

	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
		page.getParticle(Locators.FAQPageMolecola.HEADER).visibilityOfElement(20L);
		page.getParticle(Locators.FAQPageMolecola.CANCELLBUTTON).visibilityOfElement(null);

//		Utility.swipeToElement((AppiumDriver<?>)page.getDriver(),Utility.DIRECTION.DOWN,300,"//*[resource-id='posteitaliane.posteapp.appbpol:id/tvFooterCopyright']",10);
//		Utility.swipeToElement((AppiumDriver<?>)page.getDriver(),Utility.DIRECTION.DOWN,300,"//*[resource-id='posteitaliane.posteapp.appbpol:id/tvFooterCopyright']",10);

		UIUtils.mobile().swipe((MobileDriver) page.getDriver(), UIUtils.SCROLL_DIRECTION.DOWN, 200);
		UIUtils.mobile().swipe((MobileDriver) page.getDriver(), UIUtils.SCROLL_DIRECTION.DOWN, 200);
		UIUtils.mobile().swipe((MobileDriver) page.getDriver(), UIUtils.SCROLL_DIRECTION.DOWN, 200);
		UIUtils.mobile().swipe((MobileDriver) page.getDriver(), UIUtils.SCROLL_DIRECTION.DOWN, 200);
		
		page.getParticle(Locators.FAQPageMolecola.HELPSUBSECTION).visibilityOfElement(null);
		page.getParticle(Locators.FAQPageMolecola.CONTACTBUTTON).visibilityOfElement(null);
		//page.getParticle(Locators.FAQPageMolecola.CHATWITHUSBUTTON).visibilityOfElement(null);
		page.getParticle(Locators.FAQPageMolecola.APPUFFICIOPOSTALELINK).visibilityOfElement(null);
		page.getParticle(Locators.FAQPageMolecola.APPPOSTEPAYLINK).visibilityOfElement(null);

	}

	public NumeriUtiliPage gotoNumeriUtili() 
	{
		this.contactButton=page.getParticle(Locators.FAQPageMolecola.CONTACTBUTTON).getElement();
		


		this.contactButton.click();

		NumeriUtiliPage p=(NumeriUtiliPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("NumeriUtiliPage", page.getDriver().getClass(), page.getLanguage());

		p.checkPage();

		return p;
	}


	public PosteChatPage clickOnChatWithUsButton() {

		this.chatWithUsButton=page.getParticle(Locators.FAQPageMolecola.CHATWITHUSBUTTON).getElement();
		
		this.chatWithUsButton.click();

		PosteChatPage p = (PosteChatPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("PosteChatPage", page.getDriver().getClass(), page.getLanguage());
		p.checkPage();
		return p;
	}


	public void clickOnXButton() {
		
		this.chatWithUsButton=page.getParticle(Locators.FAQPageMolecola.CANCELLBUTTON).getElement();
		

		this.chatWithUsButton.click();

	}

	public NumeriUtiliPage clickOnContactUsButton() {
		
		this.contactUsButton=page.getParticle(Locators.FAQPageMolecola.CONTACTBUTTON).getElement();
		

		this.contactUsButton.click();

		NumeriUtiliPage p = (NumeriUtiliPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("NumeriUtiliPage", page.getDriver().getClass(), page.getLanguage());
		p.checkPage();
		return p;

	}

	@Override
	public void init(UiPage page) 
	{
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) 
	{
		load();
	}

	@Override
	public void clickCancelButton() {
		this.chatWithUsButton=page.getParticle(Locators.FAQPageMolecola.CANCELLBUTTON).getElement();
		this.chatWithUsButton.click();
	}
}