package it.poste.bancoposta.pages.managers;

import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import it.poste.bancoposta.pages.Locators;

@PageManager(page="NewLoginPreLoginPage")
@IOS
public class NewLoginPreLoginPageManIOS extends NewLoginPreLoginPageMan 
{
	public void makeAnewLogin()
	{

		this.nonSeiTuButton=page.getParticle(Locators.NewLoginPreLoginPageMolecola.NONSEITUBUTTON).getElement();
		//Assert.//AssertTrue(nonSeiTuButton.isDisplayed());
		nonSeiTuButton.click();
		WaitManager.get().waitShortTime(); 

	}

}
