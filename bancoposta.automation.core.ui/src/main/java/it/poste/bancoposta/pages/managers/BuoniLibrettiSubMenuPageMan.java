package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.uiobject.UiPage;
import it.poste.bancoposta.pages.BuoniLibrettiSubMenuPage;
import it.poste.bancoposta.pages.Locators;
import test.automation.core.UIUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="BuoniLibrettiSubMenuPage")
@Android
@IOS
public class BuoniLibrettiSubMenuPageMan extends LoadableComponent<BuoniLibrettiSubMenuPageMan> implements BuoniLibrettiSubMenuPage {
	
	UiPage page;
	
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement closeButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */	
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement buoniButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement closesupersmartButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */	
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement girofondoButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */	
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement chargeYourPostePayButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement bonificoButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */	
	
	
	@Override
	protected void isLoaded() throws Error {
		
	}
	
	@Override
	protected void load() 
	{
		page.get();
	}

	@Override
	public void init(UiPage page) 
	{
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) {
		load();
		
	}
}
