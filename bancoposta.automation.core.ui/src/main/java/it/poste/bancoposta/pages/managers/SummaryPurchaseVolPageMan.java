package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.TutorialPopUpBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.SummaryPurchaseVolPage;
import test.automation.core.UIUtils;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="SummaryPurchaseVol")
@Android
@IOS
public class SummaryPurchaseVolPageMan extends LoadableComponent<SummaryPurchaseVolPageMan> implements SummaryPurchaseVolPage
{
	UiPage page;

	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
		//page.getParticle(Locators.SummaryPurchaseVolMolecola.TOP).visibilityOfElement(10L);
		page.get();
	}

	public void clickAnnulla() 
	{
		page.getParticle(Locators.SummaryPurchaseVolMolecola.CLOSE_BUTTON).visibilityOfElement(10L).click();

		WaitManager.get().waitShortTime();

		page.getParticle(Locators.DeviceNativeElementMolecola.OK_BUTTON).visibilityOfElement(10L).click();

	}

	public void clickCancellaAcquisto() 
	{
		
		UIUtils.mobile().swipe((MobileDriver) page.getDriver(), UIUtils.SCROLL_DIRECTION.DOWN, 0.5f, 0.5f, 0.5f, 0.2f, 300);

		WaitManager.get().waitMediumTime();

		page.getParticle(Locators.SummaryPurchaseVolMolecola.ANNULLA_BUTTON).visibilityOfElement(10L).click();
		
		WaitManager.get().waitMediumTime();

		page.readPageSource();
		page.getParticle(Locators.SummaryPurchaseVolMolecola.PROSEGUI_CANCELLAZIONE_BUTTON).visibilityOfElement(10L).click();

	}

	@Override
	public void init(UiPage page) {
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) {
		load();
	}

}
