package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.uiobject.UiPage;
import it.poste.bancoposta.pages.EditOperationPage;
import it.poste.bancoposta.pages.Locators;
import test.automation.core.UIUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="EditOperationPage")
@Android
@IOS
public class EditOperationPageMan extends LoadableComponent<EditOperationPageMan> implements EditOperationPage
{
	UiPage page;
	

	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement header;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement leftmenu;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement searchInput;
	/**
	 * @android campo dinamico sostituire $(tabName) con il nome del tab da ricercare
	 * @ios campo dinamico sostituire $(tabName) con il nome del tab da ricercare
	 * @web 
	 */
	private WebElement categoryTab;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement categoriesList;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement categoryCurrent;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement categoryElement;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement categorySelectedFlag;

	@Override
	protected void isLoaded() throws Error {
	}

	@Override
	protected void load() 
	{
		page.get();
	}

	@Override
	public void init(UiPage page) 
	{
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) 
	{
		load();
	}
}
