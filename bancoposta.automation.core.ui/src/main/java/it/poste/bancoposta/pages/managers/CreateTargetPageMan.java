package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.CredentialAccessBean;
import io.appium.java_client.android.AndroidDriver;
import it.poste.bancoposta.pages.BonificoConfirmSubPage;
import it.poste.bancoposta.pages.CreateTargetPage;
import it.poste.bancoposta.pages.DurationTargetPage;
import it.poste.bancoposta.pages.Locators;
import test.automation.core.UIUtils;

import java.sql.Date;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
@PageManager(page="CreateTargetPage")
@Android
@IOS
public class CreateTargetPageMan extends LoadableComponent<CreateTargetPageMan> implements CreateTargetPage
{

	UiPage page;

	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement headerTarget;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement cancelButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement descriptionPage;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	private WebElement helpButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement categoryImageIcon;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */    

	private WebElement categoryName;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */  

	private List<WebElement> record;

	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement objective;

	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
		page.getParticle(Locators.SalvadanaioPageMolecola.HEADERTARGET).visibilityOfElement(20L);
		page.getParticle(Locators.SalvadanaioPageMolecola.CANCELBUTTON).visibilityOfElement(null);
		page.getParticle(Locators.SalvadanaioPageMolecola.DESCRIPTIONPAGE).visibilityOfElement(null);

	}



	public DurationTargetPage selectObjective() 
	{
		objective = page.getParticle(Locators.SalvadanaioPageMolecola.CATEGORYIMAGEICON).getElement();
		

		//String ob= objectiveParam.trim().toLowerCase();  

		//List<WebElement> list=driver.findElements(By.xpath(objective)); 
		// System.out.println(list);
		//String result=e.findElement(By.xpath(objective)).getText().trim().toLowerCase();

		/*if(ob.equals(result))
		{ System.out.println(result+ "result nel for");
			e.click();
			break;

		}else {
			       																    //  P1X P1Y  Swipe P2X SwipeP2Y
			//UIUtils.mobile().swipe((MobileDriver) driver, UIUtils.SCROLL_DIRECTION.LEFT,0.5f,0.5f,0.3f,0.5f,200); 

			try {Thread.sleep(2000); } catch (Exception err) {} /*/

		objective.click();

		DurationTargetPage d = (DurationTargetPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("DurationTargetPage", page.getDriver().getClass(), page.getLanguage());
		d.checkPage();

		return d;

	}

	@Override
	public void init(UiPage page) 
	{
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) 
	{
		load();
	}
}
