package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.w3c.dom.NodeList;

import android.Utility;
import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.BiscottoDataBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileDriver;
import it.poste.bancoposta.pages.BonificoConfirmSubPage;
import it.poste.bancoposta.pages.BuoniLibrettiPage;
import it.poste.bancoposta.pages.FAQPage;
import it.poste.bancoposta.pages.HomePage;
import it.poste.bancoposta.pages.HomepageHamburgerMenu;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.QuickOperationListPage;
import it.poste.bancoposta.pages.YourExpencesPage;
import it.poste.bancoposta.pages.YourExpencesWizardPage;
import test.automation.core.UIUtils;
import utils.ObjectFinderLight;

import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.Point;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.Dimension;
@PageManager(page="HomePage")
@Android
public class HomePageMan extends LoadableComponent<HomePageMan> implements HomePage
{
	UiPage page;
	protected static final String CHIUDI_MENU = "Chiudi menu";
	/**
	 * @android 
	 * @ios campo dinamico sostituire $(username) con il valore user name da ricercare
	 * @web 
	 */
	protected WebElement welcomeHeader;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement bancopostaHeader;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement leftMenu;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement yourProductTab;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement yourBillsTabs;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement quickPaimentsTab;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement quickPaimentsShowAllLink;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement quickPaimentsList;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement quickPaimentsElement;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement billAndCardsTab;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement billAndCardsShowAccountsLink;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement billAndCardsList;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement billAndCardElement;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement buoniELibrettiElement;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement buoniELibrettiButtonLink;

	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement faqLink;



	protected String userHeader;
	protected WebElement chiudiMenu;
	protected WebElement faqPage;
	protected WebElement faqPageButton;
	protected WebElement siAccettoButton;
	protected WebElement allowPopupButton;
	protected WebElement biscotto;

	@Override
	protected void isLoaded() throws Error {

	}

	@Override
	protected void load() 
	{
		page.get();

		//controllo che il welcome header contenga la Stringa "Ciao userHeader!"
		String welcHeader=page.getParticle(Locators.HomePageMolecola.WELCOMEHEADER).getElement().getText();
		String checkWelcHeader="Ciao "+this.userHeader+"!";

		Assert.assertTrue("controllo header home page",welcHeader.equals(checkWelcHeader));
		
//		byte[] b=((TakesScreenshot)page.getDriver()).getScreenshotAs(OutputType.BYTES);
//		PageMolecolaImgCreator.makeImg(b, page, 
//				Locators.HomePageMolecola.WELCOMEHEADER,
//				Locators.HomePageMolecola.BANCOPOSTAHEADER,
//				Locators.HomePageMolecola.LEFTMENU,
//				Locators.HomePageMolecola.YOURPRODUCTTAB,
//				Locators.HomePageMolecola.YOURBILLSTABS,
//				Locators.HomePageMolecola.QUICKPAIMENTSTAB,
//				Locators.HomePageMolecola.QUICKPAIMENTSSHOWALLLINK,
//				Locators.HomePageMolecola.QUICKPAIMENTSLIST,
//				Locators.HomePageMolecola.BILLANDCARDSSHOWACCOUNTSLINK,
//				Locators.HomePageMolecola.BILLANDCARDSLIST,
//				Locators.HomePageMolecola.FAQLINK);
	}

	public void setUserHeader(String userHeader) 
	{
		this.userHeader=userHeader;
	}

	public HomepageHamburgerMenu openHamburgerMenu() 
	{
		WaitManager.get().waitMediumTime();
		
		this.leftMenu=page.getParticle(Locators.HomePageMolecola.LEFTMENU).visibilityOfElement(10L);
		

		this.leftMenu.click();

		HomepageHamburgerMenu h=(HomepageHamburgerMenu) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("HomepageHamburgerMenu", page.getDriver().getClass(), page.getLanguage());

		h.checkPage();

		return h;
	}

	public void scrollToBuonieLibrettiSection() 
	{
		//viene effettuato uno scroll alla sezione buoni e libretti
		/*
			for(int i=0;i<5;i++)
			{
				Utility.swipe((MobileDriver) driver, Utility.DIRECTION.DOWN, 200);

				try
				{
					this.buoniELibrettiElement=driver.findElement(By.xpath(Locators.HomePageMolecola.BUONIELIBRETTIELEMENT.getAndroidLocator()));

					break;
				}
				catch(Exception err)
				{

				}
			}*/
		Utility.swipeToElement((MobileDriver) page.getDriver(), Utility.DIRECTION.DOWN, 200,"//*[@text='Buoni e Libretti']",10);

	}

	public BuoniLibrettiPage gotoBuoniELibrettiPage() 
	{
		this.buoniELibrettiElement=page.getParticle(Locators.HomePageMolecola.BUONIELIBRETTIELEMENT).getElement();
		

		this.buoniELibrettiElement.click();


		BuoniLibrettiPage p=(BuoniLibrettiPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("BuoniLibrettiPage", page.getDriver().getClass(), page.getLanguage());

		p.checkPage();

		return p;
	}

	public FAQPage gotoFaq() 
	{
		this.faqLink=page.getParticle(Locators.HomePageMolecola.FAQLINK).getElement();
		

		this.faqLink.click();

		FAQPage p=(FAQPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("FAQPage", page.getDriver().getClass(), page.getLanguage());

		p.checkPage();


		return p;
	}

	public void clickOnOperazioniVeloci()
	{
		WaitManager.get().waitHighTime();
		
		this.quickPaimentsShowAllLink=page.getParticle(Locators.HomePageMolecola.QUICKPAIMENTSSHOWALLLINK).getElement();
		

		this.quickPaimentsShowAllLink.click();
	}

	public QuickOperationListPage gotoOperazioniVeloci() 
	{
		clickOnOperazioniVeloci();

		QuickOperationListPage p=(QuickOperationListPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("QuickOperationListPage", page.getDriver().getClass(), page.getLanguage());

		p.checkPage();

		return p;
	}

	public YourExpencesPage gotoLeTueSpese() 
	{
		this.yourBillsTabs=page.getParticle(Locators.HomePageMolecola.YOURBILLSTABS).getElement();
		

		this.yourBillsTabs.click();

		//controllo che sia mostrato il wizard le tue spese,
		//esso viene mostrato solo al primo accesso alla sezione
		YourExpencesWizardPage wizard=(YourExpencesWizardPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("YourExpencesWizardPage", page.getDriver().getClass(), page.getLanguage());

		WaitManager.get().waitMediumTime();

		try
		{
			//wizard.get();
			wizard.clickContinua();
			Thread.sleep(1500);
			wizard.clickContinua();
			Thread.sleep(1500);
			wizard.clickContinua();
			Thread.sleep(1500);
			wizard.clickContinua();
			Thread.sleep(1500);
			wizard.clickContinua();
			Thread.sleep(1500);
		}
		catch(Exception err)
		{

		}
		
		try
		{
			//wizard.get();
			wizard.clickSalta();
			Thread.sleep(1500);
			wizard.clickSalta();
			Thread.sleep(1500);
			wizard.clickSalta();
			Thread.sleep(1500);
			wizard.clickSalta();
			Thread.sleep(1500);
			wizard.clickSalta();
			Thread.sleep(1500);
		}
		catch(Exception err)
		{

		}

		YourExpencesPage p=(YourExpencesPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("YourExpencesPage", page.getDriver().getClass(), page.getLanguage());

		p.checkPage();

		return p;
	}

	public void navigaVersoIlBiscotto(BiscottoDataBean b) 
	{
		//TODO:TAA-313
		//		WebElement e = driver.findElement(By.xpath(BiscottoHomeMolecola.BISCOTTI_PAGE_INDICATOR.getLocator(driver)));
		//		int size=e.findElements(By.xpath(BiscottoHomeMolecola.PAGE_INDICATOR_PIN.getLocator(driver))).size();
		//		System.out.println("numero biscotti identificati:"+size);
		//
		//		for(int i=0;i<size;i++)
		//		{
		//			WebElement biscotto=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(BiscottoHomeMolecola.TESTO.getLocator(driver))));
		//			String text=biscotto.getText();
		//
		//			System.out.println("biscotto visibile:"+text);
		//
		//			if(text.contains(b.getBiscotto()))
		//			{
		//
		//				return;
		//			}
		//			else
		//			{
		//				UIUtils.mobile().swipe((MobileDriver) driver, UIUtils.SCROLL_DIRECTION.RIGHT, 0.5f, 0.2f, 0f, 0.2f, 1000);
		//			}
		//
		//			WaitManager.get().waitShortTime();
		//		}

		String pageSource=page.getDriver().getPageSource();
		NodeList biscotti=ObjectFinderLight.getElements(pageSource, page.getParticle(Locators.BiscottoHomeMolecola.SCOPRI_BUTTON).getLocator());
		System.out.println("biscotti trovati:"+biscotti.getLength());

		int size=12;

		WebElement cruscotto = page.getParticle(Locators.BiscottoHomeMolecola.cruscottoBiscotti).visibilityOfElement(10L);
		Rectangle r = cruscotto.getRect();
		Point p = cruscotto.getLocation();
		float y = p.y+r.height*0.5f;
		Dimension screen = page.getDriver().manage().window().getSize();
		y=y/(screen.getHeight()*1.0f);
		System.out.println(y);

		for(int i=0;i<size;i++)
		{
			biscotto=page.getParticle(Locators.BiscottoHomeMolecola.TESTO).visibilityOfElement(10L);
			String text=biscotto.getText();

			System.out.println("biscotto visibile:"+text);

			if(text.contains(b.getBiscotto()))
			{
				return;
			}
			else
			{
				UIUtils.mobile().swipe((MobileDriver) page.getDriver(), UIUtils.SCROLL_DIRECTION.RIGHT, 0.5f, y, 0f, y, 1000);
			}

			WaitManager.get().waitShortTime();
		}
		//TODO:TAA-313
	}

	public void clickOnScopriDaBiscotto() 
	{
		//TODO:TAA-313
		//WebElement scopri=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(BiscottoHomeMolecola.SCOPRI_BUTTON.getLocator(driver))));
		//scopri.click();
		WaitManager.get().waitShortTime();
		biscotto.click();
		//TODO:TAA-313
	}

	public void checkContoInApertura() 
	{
		page.getParticle(Locators.HomePageMolecola.CONTO_IN_APERTURA).visibilityOfElement(10L);
	}

	public void clickOnContoInApertura() 
	{
		page.getParticle(Locators.HomePageMolecola.CONTO_IN_APERTURA).visibilityOfElement(10L).click();
	}

	@Override
	public void init(UiPage page) 
	{
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) 
	{
		load();
	}

	
}
