package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.Entry;
import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.Web;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.CredentialAccessBean;
import bean.datatable.PaymentCreationBean;
import bean.datatable.NotificaPushBean;
import it.poste.bancoposta.pages.BPOLPage;
import it.poste.bancoposta.pages.Locators;
import test.automation.core.UIUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="BPOLApp")
@Web
public class BPOLPageMan extends LoadableComponent<BPOLPageMan> implements BPOLPage
{
	UiPage page;

	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
		UIUtils.ui().waitForCondition(page.getDriver(), ExpectedConditions.visibilityOfElementLocated(page.getParticle(Locators.BPOLAppMolecola.AREAPERSONALE).getXPath()), 20);
		

	}

	public void gotoAreaPersonale() 
	{
		WebDriver driver=page.getDriver();
		try
		{
			WebElement cookie=UIUtils.ui().waitForCondition(page.getDriver(), ExpectedConditions.visibilityOfElementLocated(page.getParticle(Locators.BPOLAppMolecola.COOKIEBUTTON).getXPath()), 3);
			cookie.click();
		}
		catch(Exception err)
		{

		}
		WebElement areaPersonale=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, page.getParticle(Locators.BPOLAppMolecola.AREAPERSONALE).getXPath());
		WaitManager.get().waitMediumTime();
		areaPersonale=page.getParticle(Locators.BPOLAppMolecola.AREAPERSONALE).getElement();
		areaPersonale.click();
	}

	public void login(CredentialAccessBean b) 
	{
		WebElement username=page.getParticle(Locators.BPOLAppMolecola.USERNAME).scrollAndReturnElement();
		WaitManager.get().waitMediumTime();
		username.clear();
		username.sendKeys(b.getUsername());
		
		WebElement password=page.getParticle(Locators.BPOLAppMolecola.PASSWORD).scrollAndReturnElement();
		WaitManager.get().waitMediumTime();
		password.clear();
		password.sendKeys(b.getPassword());
		
		WebElement entra=page.getParticle(Locators.BPOLAppMolecola.ACCEDI).scrollAndReturnElement();
		WaitManager.get().waitMediumTime();
		entra.click();
	}

	public void gotoBancoPostaOnline() 
	{
		UIUtils.ui().waitForCondition(page.getDriver(), ExpectedConditions.visibilityOfElementLocated(page.getParticle(Locators.BPOLAppMolecola.SERVIZIONLINE).getXPath()), 20);
		//UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.BPOLAppMolecola.SERVIZIONLINE.getWebLocator()));
		WaitManager.get().waitLongTime();
		WebElement servizi;
		try
		{
			servizi=page.getParticle(Locators.BPOLAppMolecola.SERVIZIONLINE).getElement();
		}
		catch(Exception er)
		{
			servizi=page.getParticle(Locators.BPOLAppMolecola.SERVIZIONLINE).getElement();
		}
		servizi.click();
		WaitManager.get().waitHighTime();
		WebElement contoCarte=page.getParticle(Locators.BPOLAppMolecola.CONTOECARTEFINANZIAMENTI).getElement();
		contoCarte.click();
		WaitManager.get().waitShortTime();
		WebElement bancoPostaOnline=page.getParticle(Locators.BPOLAppMolecola.BANCOPOSTAONLINELINK).getElement();
		bancoPostaOnline.click();
	}

	public void inviaNotificaPush() 
	{
		UIUtils.ui().waitForCondition(page.getDriver(), ExpectedConditions.visibilityOfElementLocated(page.getParticle(Locators.BPOLAppMolecola.ABILITAHEADER).getXPath()), 20);
		
		WebElement bpRadio=page.getParticle(Locators.BPOLAppMolecola.AUTORIZZABPRADIO).scrollAndReturnElement();
		WaitManager.get().waitMediumTime();
		bpRadio.click();
		
		WebElement procedi=page.getParticle(Locators.BPOLAppMolecola.PROSEGUI).scrollAndReturnElement();
		WaitManager.get().waitMediumTime();
		procedi.click();
		
		UIUtils.ui().waitForCondition(page.getDriver(), ExpectedConditions.visibilityOfElementLocated(page.getParticle(Locators.BPOLAppMolecola.MODALPUSHHEADER).getXPath()), 20);
		
	}

	public void checkIsBancoPostaPage() 
	{
		try {
			page.getParticle(Locators.BPOLAppMolecola.COOKIEBUTTON2).scrollAndReturnElement().click();
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		page.getParticle(Locators.BPOLAppMolecola.BPHPHEADER).visibilityOfElement(20L);
	}

	public void gotoContoCarteFromBP() 
	{
		WebElement procedi=page.getParticle(Locators.BPOLAppMolecola.CONTOECARTELINK).getElement();
		WaitManager.get().waitMediumTime();
		procedi.click();
		
		page.getParticle(Locators.BPOLAppMolecola.CONTOECARTEHEADER).visibilityOfElement(20L);
	}

	public void navigaBersoPostagiro() 
	{
		page.getParticle(Locators.BPOLAppMolecola.INVIO_DENARO).scrollAndReturnElement().click();
		WaitManager.get().waitMediumTime();
		page.getParticle(Locators.BPOLAppMolecola.POSTAGIRO_DIV).scrollAndReturnElement().click();
		WaitManager.get().waitLongTime();
	}

	public void compilaPostagiro(PaymentCreationBean p) 
	{
		page.getParticle(Locators.BPOLAppMolecola.POSTAGIRO_IBAN).scrollAndReturnElement().sendKeys(p.getIban());
		page.getParticle(Locators.BPOLAppMolecola.POSTAGIRO_INTESTAZIONE).scrollAndReturnElement().sendKeys(p.getOwner());
		page.getParticle(Locators.BPOLAppMolecola.POSTAGIRO_IMPORTO).scrollAndReturnElement().sendKeys(p.getAmount());
		try {
			page.getParticle(Locators.BPOLAppMolecola.POSTAGIRO_CAUSALE).scrollAndReturnElement().sendKeys(p.getDescription());
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		page.getParticle(Locators.BPOLAppMolecola.POSTAGIRO_CONTINUA).scrollAndReturnElement().click();
		WaitManager.get().waitLongTime();
		page.getParticle(Locators.BPOLAppMolecola.POSTAGIRO_PROSEGUI).scrollAndReturnElement().click();
		WaitManager.get().waitLongTime();
	}

	public void checkOperationNotificaOK(NotificaPushBean b) 
	{
		WaitManager.get().waitLongTime();
		try {
			page.getParticle(Locators.BPOLAppMolecola.CHIUDIMODALE).scrollAndReturnElement().click();
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		page.getParticle(Locators.BPOLAppMolecola.OPERAZIONEOK).scrollAndReturnElement(new Entry("text",b.getTitle()));

		
	}

	@Override
	public void init(UiPage page) 
	{
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) 
	{
		load();
	}

	@Override
	public void setDriver(WebDriver bpol) {
		
	}

}
