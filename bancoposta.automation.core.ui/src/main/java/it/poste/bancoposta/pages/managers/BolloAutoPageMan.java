package it.poste.bancoposta.pages.managers;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Utility;
import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.BolloAutoBean;
import bean.datatable.BonificoDataBean;
import bean.datatable.QuickOperationCreationBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.android.AndroidDriver;
import it.poste.bancoposta.pages.BolloAutoConfirmPage;
import it.poste.bancoposta.pages.BolloAutoPage;
import it.poste.bancoposta.pages.FAQPage;
import it.poste.bancoposta.pages.Locators;
import test.automation.core.UIUtils;

import static org.junit.Assert.assertTrue;

import java.util.Locale;

import org.apache.poi.xwpf.usermodel.TOC;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
@PageManager(page="BolloAutoPage")
@Android
@IOS
public class BolloAutoPageMan extends LoadableComponent<BolloAutoPageMan> implements BolloAutoPage 
{
	UiPage page;


	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement header;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement cancelButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement assistenza;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement region;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement licensePlate;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement vehicle;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement calculate;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement previousYear;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement month;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement year;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement validyMonths;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement pregressaButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement veicoloButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement campaniaButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement pregressa;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement monthPickerButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement yearPickerButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement validityMonthsPickerButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	@Override
	protected void isLoaded() throws Error 
	{

	}


	@Override
	protected void load() 
	{
		page.get();
	}

	public BolloAutoConfirmPage calculateBollo(BolloAutoBean b) {
		BolloAutoPage bolloAutoPage = (BolloAutoPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("BolloAutoPage", page.getDriver().getClass(), page.getLanguage());

		this.region=page.getParticle(Locators.BolloAutoPageMolecola.REGIONBUTTON).getElement();		
		this.licensePlate=page.getParticle(Locators.BolloAutoPageMolecola.LICENSEPLATEBUTTON).getElement();			 
		this.vehicle=page.getParticle(Locators.BolloAutoPageMolecola.VEHICLEBUTTON).getElement();
		this.calculate=page.getParticle(Locators.BolloAutoPageMolecola.CALCULATEBUTTON).getElement();


		this.region.click();

		this.campaniaButton=page.getParticle(Locators.BolloAutoPageMolecola.CAMPANIABUTTON).getElement();


		this.campaniaButton.click();

		WaitManager.get().waitMediumTime();

		this.licensePlate.sendKeys(b.getLicensePlate());


		WaitManager.get().waitMediumTime();

		this.vehicle.click();

		WaitManager.get().waitMediumTime();

		this.veicoloButton=page.getParticle(Locators.BolloAutoPageMolecola.QUADRICICLOBUTTON).getElement();

		this.veicoloButton.click();

		WaitManager.get().waitMediumTime();

		if (b.getOldPayment().contentEquals("yes")) 
		{
			this.pregressa=page.getParticle(Locators.BolloAutoPageMolecola.PREGRESSABUTTON).getElement();						
			this.pregressa.click();
			this.month=page.getParticle(Locators.BolloAutoPageMolecola.MONTHBUTTON).getElement();			 
			this.year=page.getParticle(Locators.BolloAutoPageMolecola.YEARBUTTON).getElement();
			this.validyMonths=page.getParticle(Locators.BolloAutoPageMolecola.VALIDITYMONTHBUTTON).getElement();


			this.month.click();

			this.monthPickerButton=page.getParticle(Locators.BolloAutoPageMolecola.MONTPICKERBUTTON).getElement();
			String monthText = monthPickerButton.getText().trim();
			this.monthPickerButton.click();

			BolloAutoPage bolloAutoPage1 = (BolloAutoPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("BolloAutoPage", page.getDriver().getClass(), page.getLanguage());

			bolloAutoPage1.checkPage();

			this.month=page.getParticle(Locators.BolloAutoPageMolecola.MONTHBUTTON).getElement();			 
			Assert.assertTrue(month.getText().trim().equals(monthText));
			this.year.click();

			this.yearPickerButton=page.getParticle(Locators.BolloAutoPageMolecola.YEARPICKERBUTTON).getElement();
			String yearText = yearPickerButton.getText().trim();
			this.yearPickerButton.click();
			bolloAutoPage1 = (BolloAutoPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("BolloAutoPage", page.getDriver().getClass(), page.getLanguage());

			this.year=page.getParticle(Locators.BolloAutoPageMolecola.YEARBUTTON).getElement();
			Assert.assertTrue(year.getText().trim().equals(yearText));
			this.validyMonths.click();
			this.validityMonthsPickerButton=page.getParticle(Locators.BolloAutoPageMolecola.VALIDITYMONTHSPICKERBUTTON).getElement();
			String validityMonthsText = validityMonthsPickerButton.getText().trim();
			this.validityMonthsPickerButton.click();
			bolloAutoPage1 = (BolloAutoPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("BolloAutoPage", page.getDriver().getClass(), page.getLanguage());

			bolloAutoPage1.checkPage();

			this.validyMonths=page.getParticle(Locators.BolloAutoPageMolecola.VALIDITYMONTHBUTTON).getElement();
			Assert.assertTrue(validyMonths.getText().trim().equals(validityMonthsText));

		} else if (b.getOldPayment().contentEquals("check")) 
		{
			this.pregressa=page.getParticle(Locators.BolloAutoPageMolecola.PREGRESSABUTTON).getElement();						
			this.pregressa.click();
			this.month=page.getParticle(Locators.BolloAutoPageMolecola.MONTHBUTTON).getElement();			 
			this.year=page.getParticle(Locators.BolloAutoPageMolecola.YEARBUTTON).getElement();
			this.validyMonths=page.getParticle(Locators.BolloAutoPageMolecola.VALIDITYMONTHBUTTON).getElement();


			this.month.click();

			this.monthPickerButton=page.getParticle(Locators.BolloAutoPageMolecola.MONTPICKERBUTTON).getElement();
			String monthText = monthPickerButton.getText().trim();
			this.monthPickerButton.click();

			BolloAutoPage bolloAutoPage1 = (BolloAutoPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("BolloAutoPage", page.getDriver().getClass(), page.getLanguage());

			bolloAutoPage1.checkPage();

			this.month=page.getParticle(Locators.BolloAutoPageMolecola.MONTHBUTTON).getElement();			 
			Assert.assertTrue(month.getText().trim().equals(monthText));

			this.year.click();

			this.yearPickerButton=page.getParticle(Locators.BolloAutoPageMolecola.YEARPICKERBUTTON).getElement();
			String yearText = yearPickerButton.getText().trim();
			this.yearPickerButton.click();
			bolloAutoPage1 = (BolloAutoPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("BolloAutoPage", page.getDriver().getClass(), page.getLanguage());

			bolloAutoPage1.checkPage();

			this.year=page.getParticle(Locators.BolloAutoPageMolecola.YEARBUTTON).getElement();
			Assert.assertTrue(year.getText().trim().equals(yearText));

			this.validityMonthsPickerButton=page.getParticle(Locators.BolloAutoPageMolecola.VALIDITYMONTHSPICKERBUTTON).getElement();
			String validityMonthsText = validityMonthsPickerButton.getText().trim();
			this.validityMonthsPickerButton.click();

			bolloAutoPage1 = (BolloAutoPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("BolloAutoPage", page.getDriver().getClass(), page.getLanguage());

			bolloAutoPage1.checkPage();
			this.validyMonths=page.getParticle(Locators.BolloAutoPageMolecola.VALIDITYMONTHBUTTON).getElement();
			Assert.assertTrue(validyMonths.getText().trim().equals(validityMonthsText));


			this.pregressa.click();

		}

		this.calculate.click();

		BolloAutoConfirmPage p = (BolloAutoConfirmPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("BolloAutoConfirmPage", page.getDriver().getClass(), page.getLanguage());

		p.checkPage();

		return p;

	}


	public BolloAutoPage checkAssistenzaPage() {
		BolloAutoPage bolloAutoPage = (BolloAutoPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("BolloAutoPage", page.getDriver().getClass(), page.getLanguage());

		bolloAutoPage.checkPage();

		WaitManager.get().waitMediumTime();

		this.assistenza=page.getParticle(Locators.BolloAutoPageMolecola.ASSISTENZABUTTON).getElement();		

		WaitManager.get().waitMediumTime();

		this.assistenza.click();

		FAQPage p=(FAQPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("FAQPage", page.getDriver().getClass(), page.getLanguage());
		p.checkPage();
		
		System.out.println("La pagina � giusta");
		
		p.clickCancelButton();

//		this.cancelButton=driver.findElement(By.xpath(Locators.FAQPageMolecola.CANCELLBUTTON.getLocator(driver)));		
//		
//
//		this.cancelButton.click();

		BolloAutoPage c = (BolloAutoPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("BolloAutoPage", page.getDriver().getClass(), page.getLanguage());
		
		c.checkPage();

		return c;




	}

	@Override
	public void init(UiPage page) 
	{
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) 
	{
		load();
	}

}


