package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Utility;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.BolloAutoBean;
import bean.datatable.BonificoDataBean;
import bean.datatable.QuickOperationCreationBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.android.AndroidDriver;
import it.poste.bancoposta.pages.BolloAutoConfirmPage;
import it.poste.bancoposta.pages.BolloAutoRiepilogoPage;
import it.poste.bancoposta.pages.Locators;
import test.automation.core.UIUtils;

import java.util.Locale;

import org.apache.poi.xwpf.usermodel.TOC;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
@PageManager(page="BolloAutoConfirmPage")
@Android
@IOS
public class BolloAutoConfirmPageMan extends LoadableComponent<BolloAutoConfirmPageMan> implements BolloAutoConfirmPage
{
	UiPage page;
	
	
	private WebElement header;
	private WebElement backbutton;
	private WebElement assisstenzaButton;
	private WebElement totaleDaPagare;
	private WebElement totaleImporto;
	private WebElement targa;
	private WebElement veicolo;
	private WebElement pagaCon;
	private WebElement confermaButton;
	
	
	
	
	
	public WebElement getTotaleImporto() {
		return totaleImporto;
	}

	public void setTotaleImporto(WebElement totaleImporto) {
		this.totaleImporto = totaleImporto;
	}
	
	@Override
	protected void isLoaded() throws Error {
		
	}
	
	
	@Override
	protected void load() 
	{
		page.get();
	}
	
	
	
	
	public void checkData(BolloAutoBean b) {
		
		BolloAutoConfirmPage c = (BolloAutoConfirmPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("BolloAutoConfirmPage", page.getDriver().getClass(), page.getLanguage());
		
		this.header=page.getParticle(Locators.BolloAutoConfirmPageMolecola.HEADER).getElement();		
//		this.totaleDaPagare=driver.findElement(By.xpath(Locators.BolloAutoConfirmPageMolecola.TOTALEDAPAGARE.getAndroidLocator()));			 
		this.totaleImporto=page.getParticle(Locators.BolloAutoConfirmPageMolecola.TOTALEIMPORTO).getElement();
		this.targa=page.getParticle(Locators.BolloAutoConfirmPageMolecola.TARGA).getElement();
		this.veicolo=page.getParticle(Locators.BolloAutoConfirmPageMolecola.VEICOLO).getElement();
		this.pagaCon=page.getParticle(Locators.BolloAutoConfirmPageMolecola.PAGACON).getElement();
		
		
		
		Assert.assertTrue(header.getText().trim().equals("Bollo Auto e Moto"));
		System.out.println("L'header � giusto");
		
//		Assert.assertTrue(totaleDaPagare.getText().trim().equals("Totale da pagare"));
//		System.out.println("Il totale da pagare � giusto");
		
		Assert.assertTrue(totaleImporto.isDisplayed());
		System.out.println("L'importo � visualizzato");
		
		Assert.assertTrue(targa.getText().trim().equals(b.getLicensePlate()));
		try {
			this.targa.click();
			this.targa.sendKeys("Modificato");
		} catch (Exception e) {
			System.out.println("Il campo targa non � modificabile");
		}
		System.out.println("La targa � giusta");
		
		Assert.assertTrue(veicolo.getText().trim().equals(b.getVehicle().trim()));
		try {
			this.veicolo.click();
			this.veicolo.sendKeys("Moodificato");
		} catch (Exception e) {
			System.out.println("Il campo veicolo non � modificabile");
		}
		System.out.println("Il veicolo � corretto");
		
		System.out.println(pagaCon.getText().trim().toLowerCase());
		System.out.println(b.getPayWith2().trim().toLowerCase());
		
		Assert.assertTrue(pagaCon.getText().trim().toLowerCase().equals(b.getPayWith2().trim().toLowerCase()));
		System.out.println("La carta visualizzata � giusta");
				
	}
	
	
	public BolloAutoRiepilogoPage goToRiepilogo() {
		
		BolloAutoConfirmPage c = (BolloAutoConfirmPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("BolloAutoConfirmPage", page.getDriver().getClass(), page.getLanguage());
		
		this.confermaButton=page.getParticle(Locators.BolloAutoConfirmPageMolecola.CONFERMABUTTON).getElement();		
		
		this.confermaButton.click();

		BolloAutoRiepilogoPage f = (BolloAutoRiepilogoPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("BolloAutoRiepilogoPage", page.getDriver().getClass(), page.getLanguage());
		
		f.checkPage();
		return f;
	}

	@Override
	public void init(UiPage page) 
	{
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) 
	{
		load();
	}
}
