package it.poste.bancoposta.pages.managers;

import org.openqa.selenium.Rectangle;

import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import io.appium.java_client.HidesKeyboard;
import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;
import it.poste.bancoposta.pages.Locators;

@PageManager(page="LoginPosteItPage")
@IOS
public class LoginPosteItPageManIOS extends LoginPosteItPageMan 
{
	public void loginToTheApp(String username, String password) 
	{
		this.userName=page.getParticle(Locators.LoginPosteItPageMolecola.USERNAME).getElement();
		this.password=page.getParticle(Locators.LoginPosteItPageMolecola.PASSWORD).getElement();
		

		this.userName.sendKeys(username);

		try
		{
			((HidesKeyboard) page.getDriver()).hideKeyboard();
		}
		catch(Exception err)
		{

		}
		
		WaitManager.get().waitMediumTime();

		this.password.sendKeys(password);

		try
		{
			((HidesKeyboard) page.getDriver()).hideKeyboard();
		}
		catch(Exception err)
		{

		}
		
		WaitManager.get().waitMediumTime();

		this.signInButton=page.getParticle(Locators.LoginPosteItPageMolecola.SIGNINBUTTON).getElement();
		
		this.signInButton.click();

	}
}
