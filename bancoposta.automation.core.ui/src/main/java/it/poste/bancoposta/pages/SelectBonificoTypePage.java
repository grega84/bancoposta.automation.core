package it.poste.bancoposta.pages;

import automation.core.ui.pagemanager.AbstractPageManager;

public interface SelectBonificoTypePage extends AbstractPageManager {

	void clickOnBonificoSepa();

}
