package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.CredentialAccessBean;
import bean.datatable.SalvadanaioDataBean;
import io.appium.java_client.android.AndroidDriver;
import it.poste.bancoposta.pages.CloseObjectivePage;
import it.poste.bancoposta.pages.InsertPosteIDPage;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.SalvadanaioInfoObjectivePage;

import test.automation.core.UIUtils;
import java.text.SimpleDateFormat;

import java.sql.Date;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


@PageManager(page="SalvadanaioInfoObjectivePage")
@Android
@IOS
public class SalvadanaioInfoObjectivePageMan extends LoadableComponent<SalvadanaioInfoObjectivePageMan> implements SalvadanaioInfoObjectivePage
{
	UiPage page;

	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	private WebElement header;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement backButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement assistenzaButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	private WebElement shareButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	private WebElement threePointsMenuButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement currentAmount;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement totalAmount;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement textLabel
	;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement versamentiButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement creationDate;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement expirationDate;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement suggestionLabelText;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement versaButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement closeObjectiveButton;
	private WebElement closeObjectiveOkButton;
	private WebElement chiudi;
	private String cur;
	private WebElement evolution;
	private WebElement evolutionTwo;
	private WebElement owner;
	private WebElement ownerLoc;
	private String o;
	private WebElement importCheck;
	private String curTwo;
	private WebElement closeButton;
	private WebElement okButton;
	private WebElement gestisciVersamentoRicorrenteButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	@Override
	protected void isLoaded() throws Error {
		
	} 

	@Override
	protected void load() 
	{
		page.get();
	}



	public void clickOnVersaButton() 
	{

		this.versaButton=page.getParticle(Locators.SalvadanaioInfoPageMolecola.VERSABUTTON).getElement();

		this.versaButton.click();

		//			SalvadanaioDepositPage b = new SalvadanaioDepositPage(driver, driverType);
		//			b.get();
		//			


	}


	public void clickOnVersamentiListButton() {

		this.versamentiButton=page.getParticle(Locators.SalvadanaioInfoPageMolecola.VERSAMENTIBUTTON).getElement();
		

		this.versamentiButton.click();	
	}

	public void closeObjective(String posteID,String evolutionNumber,String owner) {
		
		this.currentAmount=page.getParticle(Locators.SalvadanaioInfoPageMolecola.CURRENTAMOUNT).getElement();
		this.closeObjectiveButton=page.getParticle(Locators.SalvadanaioInfoPageMolecola.CLOSEOBJECTIVEBUTTON).getElement();
		
		cur=currentAmount.getText().replace("�","").trim();

		System.out.println(cur);

		if (cur.equals("0,00")){

			this.closeObjectiveButton.click();

			this.closeObjectiveOkButton=page.getParticle(Locators.SalvadanaioInfoPageMolecola.CLOSEOBJECTIVEOKBUTTON).getElement();	
			closeObjectiveOkButton.click();

			InsertPosteIDPage i = (InsertPosteIDPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("InsertPosteIDPage", page.getDriver().getClass(), page.getLanguage());
			i.checkPage();
			i.insertPosteId(posteID);

			this.chiudi=page.getParticle(Locators.SalvadanaioInfoPageMolecola.CLOSEBUTTON).getElement();	
			chiudi.click();

		}else {
			this.closeObjectiveButton.click();

			this.evolution=page.getParticle(Locators.SalvadanaioInfoPageMolecola.EVOLUTIONCARD).getElement();	
			String evolutionSub = evolution.getText().trim();
			String evolutionSub2=evolutionSub.substring(evolutionSub.length()-4);

			String evolutionFuture = evolutionNumber.substring(evolutionNumber.length()-4);
			System.out.println(evolutionFuture);
			Assert.assertTrue(evolutionSub2.equals(evolutionFuture));

			evolution.click();
			this.evolutionTwo=page.getParticle(Locators.SalvadanaioInfoPageMolecola.EVOLUTIONCARDTWO).getElement();	
			//String evolutionSubTwo = evolutionNumber.substring(evolutionNumber.length()-4);
			//System.out.println(evolutionSubTwo);



			this.ownerLoc=page.getParticle(Locators.SalvadanaioInfoPageMolecola.OWNER).getElement();	
			String owL = ownerLoc.getText().trim().toLowerCase();
			System.out.println(owL);

			o=owner.trim().toLowerCase();
			System.out.println(o);
			Assert.assertTrue(owL.equals(o));
			System.out.println(owL + " "+ " "+ o);

			this.importCheck=page.getParticle(Locators.SalvadanaioInfoPageMolecola.IMPORT).getElement();	
			String importCheckTwo = importCheck.getText().replace("�","").trim();
			System.out.println(importCheckTwo);

			System.out.println(cur);
			Assert.assertTrue(importCheckTwo.equals(cur));


			this.closeButton=page.getParticle(Locators.SalvadanaioInfoPageMolecola.CONFIRMCLOSEBUTTON).getElement();	
			closeButton.click();

			InsertPosteIDPage i = (InsertPosteIDPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("InsertPosteIDPage", page.getDriver().getClass(), page.getLanguage());
			i.checkPage();
			i.insertPosteId(posteID);
			this.chiudi=page.getParticle(Locators.SalvadanaioInfoPageMolecola.CLOSEBUTTON).getElement();	
			chiudi.click();

		}

	}

	public void goToModifyVersamentoRicorrente() 
	{
		WaitManager.get().waitShortTime();

		page.getParticle(Locators.SalvadanaioInfoPageMolecola.SALVADANAIO_DETTAGLIO_RICORRENTE).visibilityOfElement(10L)
				.click();


		//		switch(this.driverType) 
		//		{
		//		case 0://android driver
		//			this.threePointsMenuButton=page.getParticle(Locators.SalvadanaioInfoPageMolecola.THREEPOINTSMENUBUTTON).getElement();
		//			break;
		//		case 1://ios driver
		//			this.threePointsMenuButton=page.getParticle(Locators.SalvadanaioInfoPageMolecola.THREEPOINTSMENUBUTTON).getElement();
		//			break;
		//		case 2://web driver
		//			break;
		//		}
		//		
		//		this.threePointsMenuButton.click();
		//		
		//		switch(this.driverType) 
		//		{
		//		case 0://android driver
		//			this.gestisciVersamentoRicorrenteButton=page.getParticle(Locators.SalvadanaioInfoPageMolecola.GESTISCIVERSAMENTORICORRENTEBUTTON).getElement();
		//			break;
		//		case 1://ios driver
		//			this.gestisciVersamentoRicorrenteButton=page.getParticle(Locators.SalvadanaioInfoPageMolecola.GESTISCIVERSAMENTORICORRENTEBUTTON).getElement();
		//			break;
		//		case 2://web driver
		//			break;
		//		}
		//		
		//		this.gestisciVersamentoRicorrenteButton.click();
	}

	@Override
	public void init(UiPage page) {
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) {
		load();
	}

	@Override
	public void clickBack() {
		page.getParticle(Locators.SalvadanaioInfoPageMolecola.BACKBUTTON).visibilityOfElement(10L).click();
	}

	@Override
	public void closeObiettivoV2(SalvadanaioDataBean b) {
		this.closeObjectiveButton=page.getParticle(Locators.SalvadanaioInfoPageMolecola.CLOSEOBJECTIVEBUTTON).getElement();	
		this.closeObjectiveButton.click();	

		CloseObjectivePage o =(CloseObjectivePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("CloseObjectivePage", page.getDriver().getClass(), page.getLanguage());
		o.checkPage();	
		o.closeObjectiveTwo(b.getHeader(),b.getCancel(),b.getCloseObjDescription(),b.getEvolutionNumber(),b.getPaymentMethod());			
	}


}
