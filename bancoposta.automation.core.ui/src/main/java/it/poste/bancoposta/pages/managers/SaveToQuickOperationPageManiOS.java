package it.poste.bancoposta.pages.managers;

import java.util.Properties;

import org.w3c.dom.Node;

import android.Utility;
import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import bean.datatable.QuickOperationCreationBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.SaveToQuickOperationPage;
import test.automation.core.UIUtils;
import test.automation.core.cmd.adb.AdbCommandPrompt;
import utils.ObjectFinderLight;

@PageManager(page="SaveToQuickOperationPage")
@IOS
public class SaveToQuickOperationPageManiOS extends SaveToQuickOperationPageMan 
{
	public SaveToQuickOperationPage getLight() throws Exception 
	{
		page.getQuick();

		return this;
	}
	
	public void saveAsQuickOperationLightMode(QuickOperationCreationBean b) 
	{
		page.getParticle(Locators.SaveToQuickOperationPageMolecola.QUICKOPNAMEINPUT).getElement().sendKeys(b.getQuickOperationName());
		
		WaitManager.get().waitShortTime();
		/*
		page.getParticle(Locators.SaveToQuickOperationPageMolecola.QUICKOPNAMEINPUT).getElement().sendKeys(b.getQuickOperationName());

		WaitManager.get().waitShortTime();
		*/
		try {
			((AppiumDriver<?>) page.getDriver()).hideKeyboard();
		} catch (Exception err) {
		}

		Node e=ObjectFinderLight.getElement(page.getPageSource(), page.getParticle(Locators.SaveToQuickOperationPageMolecola.CONFIRM).getLocator());

		if(e == null)
		{
			Utility.swipe((MobileDriver) page.getDriver(), Utility.DIRECTION.DOWN, 200);

			WaitManager.get().waitShortTime();

			page.readPageSource();
		}

		page.getParticle(Locators.SaveToQuickOperationPageMolecola.CONFIRM).getElement().click();

		WaitManager.get().waitShortTime();

		try {
			this.overrideQuickOperationYesButton=page.getParticle(Locators.SaveToQuickOperationPageMolecola.OVERRIDEQUICKOPERATIONYESBUTTON).getElement();
			overrideQuickOperationYesButton.click();
		} catch (Exception e2) {
			// TODO: handle exception
		}

		try {
			page.getParticle(Locators.SaveToQuickOperationPageMolecola.CONFIRM).getElement().click();
		} catch (Exception e2) {
			// TODO: handle exception
		}
	}
}
