package it.poste.bancoposta.pages;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.pagemanager.AbstractPageManager;
import bean.datatable.CredentialAccessBean;
import bean.datatable.SalvadanaioDataBean;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.android.AndroidDriver;
import test.automation.core.UIUtils;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public interface SalvadanaioDepositPage extends AbstractPageManager {


	public void checkFields(String payWith);


	public void checkFieldsAccantonamentoRicorrente(String payWith);



	public void modifyAccantonamentoRicorrente(SalvadanaioDataBean bean);

	public void modifyAccantonamentoRicorrenteDeleteAccantonamento(SalvadanaioDataBean bean);


	public void effettuaAccantonamento(String amount) ;


	public void effettuaAccantonamentoRicorrente(String amount, String payWith, String objectiveName);


}
