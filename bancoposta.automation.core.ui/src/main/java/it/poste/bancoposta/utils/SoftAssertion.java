package it.poste.bancoposta.utils;

import org.assertj.core.api.SoftAssertions;

public class SoftAssertion 
{
	private static SoftAssertion singleton;
	private SoftAssertions assertion;
	
	private SoftAssertion()
	{
		assertion=new SoftAssertions();
	}
	
	public static SoftAssertion get()
	{
		if(singleton == null)
			singleton=new SoftAssertion();
		
		return singleton;
	}
	
	public SoftAssertions getAssertion()
	{
		return this.assertion;
	}
}
