package bean.datatable;

public class QuickOperationCheckBean 
{
	private String quickOperation;
	private String page;
	private String payWith;
	public String getQuickOperation() {
		return quickOperation;
	}
	public void setQuickOperation(String quickOperation) {
		this.quickOperation = quickOperation;
	}
	public String getPage() {
		return page;
	}
	public void setPage(String page) {
		this.page = page;
	}
	public String getPayWith() {
		return payWith;
	}
	public void setPayWith(String payWith) {
		this.payWith = payWith;
	}
	
	
	
}
