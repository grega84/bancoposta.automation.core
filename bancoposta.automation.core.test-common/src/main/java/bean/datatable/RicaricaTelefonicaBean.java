package bean.datatable;

public class RicaricaTelefonicaBean {
	private String payWith;
	private String amount;
	private String testIdCheck;
	private String phoneNumber;
	private String phoneCompaign;
	private String posteid;


	public String getPayWith() {
		return payWith;
	}
	public void setPayWith(String payWith) {
		this.payWith = payWith;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getTestIdCheck() {
		return testIdCheck;
	}
	public void setTestIdCheck(String testIdCheck) {
		this.testIdCheck = testIdCheck;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getPhoneCompaign() {
		return phoneCompaign;
	}
	public void setPhoneCompaign(String phoneCompaign) {
		this.phoneCompaign = phoneCompaign;
	}
	public String getPosteid() {
		return posteid;
	}
	public void setPosteid(String posteid) {
		this.posteid = posteid;
	}



}
