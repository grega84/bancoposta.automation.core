package bean.datatable;

public class BiscottoDataBean 
{
	private String biscotto;
	private String prodotto;
	private String fidCode;
	
	

	public String getFidCode() {
		return fidCode;
	}

	public void setFidCode(String fidCode) {
		this.fidCode = fidCode;
	}

	public String getProdotto() {
		return prodotto;
	}

	public void setProdotto(String prodotto) {
		this.prodotto = prodotto;
	}

	public String getBiscotto() {
		return biscotto;
	}

	public void setBiscotto(String biscotto) {
		this.biscotto = biscotto;
	}
	
	
}
