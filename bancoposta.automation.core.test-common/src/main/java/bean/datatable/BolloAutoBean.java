package bean.datatable;

public class BolloAutoBean {

	private String region;
	private String licensePlate;
	private String vehicle;
	private String month;
	private String year;
	private String validityMonth;
	private String paymentMethod;
	private String amount;
	private String payWith;
	private String oldPayment;
	private String payWith2;
	private String commissione;
	private String posteId;
	
	
	
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getLicensePlate() {
		return licensePlate;
	}
	public void setLicensePlate(String licensePlate) {
		this.licensePlate = licensePlate;
	}
	public String getVehicle() {
		return vehicle;
	}
	public void setVehicle(String vehicle) {
		this.vehicle = vehicle;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getValidityMonth() {
		return validityMonth;
	}
	public void setValidityMonth(String validityMonth) {
		this.validityMonth = validityMonth;
	}

	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	public String getPayWith() {
		return payWith;
	}
	public void setPayWith(String payWith) {
		this.payWith = payWith;
	}
	public String getOldPayment() {
		return oldPayment;
	}
	public void setOldPayment(String oldPayment) {
		this.oldPayment = oldPayment;
	}
	public String getPayWith2() {
		return payWith2;
	}
	public void setPayWith2(String payWith2) {
		this.payWith2 = payWith2;
	}
	public String getCommissione() {
		return commissione;
	}
	public void setCommissione(String commissione) {
		this.commissione = commissione;
	}
	public String getPosteId() {
		return posteId;
	}
	public void setPosteId(String posteId) {
		this.posteId = posteId;
	}


	
	




	

}
