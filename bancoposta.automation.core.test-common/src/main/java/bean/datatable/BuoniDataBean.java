package bean.datatable;

public class BuoniDataBean {

	public static final String BUONO_FRUTTIFERO_POSTALE_BFP3X4 = "BUONO FRUTTIFERO POSTALE BFP3X4";
	public static final String BUONO_ORDINARIO = "BUONO ORDINARIO";
	public static final String BFP_4X4 = "BFP 4X4";
	public static final String BFP_3X2 = "BFP 3X2";

	private String voucherType;
	private String amount;
	private String payWith;
	
	
	public String getVoucherType() {
		return voucherType;
	}
	public void setVoucherType(String voucherType) {
		this.voucherType = voucherType;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getPayWith() {
		return payWith;
	}
	public void setPayWith(String payWith) {
		this.payWith = payWith;
	}

}


