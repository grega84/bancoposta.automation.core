package bean.datatable;

public class SalvadanaioDataBean {

	
	private String payWith;
	private String iban;
	private String cc;
	private String city;
	private String owner;
	private String amount;
	private String description;
	private String address;
	private String citySender;
	private String reference;
	private String refecenceEffective;
	private String testIdCheck;
	private String posteid;
	private String commissioni;
	private String bonificoType;
	private String beneficiario;
	private String paymentMethod;
	private String objectiveName;
    private String data;
	private String nomeObiettivo;
	private String objectiveNameNew;
	private String category;
	private String importo;
	private String importo2;
	private String evolutionNumber;
	private String quickOperationName;
	private String quickOperationName2;
	private String messageText;
	private String header;
	private String cancel;
	private String frequency;
	private String startDate;
	private String endDate;
	private String amount2;
	private String modifiedFrequency;
	private String amount3;
	private String closeObjDescription;
	private String versamentoType;
	
	
	
	
	public String getVersamentoType() {
		return versamentoType;
	}
	public void setVersamentoType(String versamentoType) {
		this.versamentoType = versamentoType;
	}
	public String getIban() {
		return iban;
	}
	public void setIban(String iban) {
		this.iban = iban;
	}
	public String getCc() {
		return cc;
	}
	public void setCc(String cc) {
		this.cc = cc;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCitySender() {
		return citySender;
	}
	public void setCitySender(String citySender) {
		this.citySender = citySender;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	public String getRefecenceEffective() {
		return refecenceEffective;
	}
	public void setRefecenceEffective(String refecenceEffective) {
		this.refecenceEffective = refecenceEffective;
	}
	public String getTestIdCheck() {
		return testIdCheck;
	}
	public void setTestIdCheck(String testIdCheck) {
		this.testIdCheck = testIdCheck;
	}
	public String getCommissioni() {
		return commissioni;
	}
	public void setCommissioni(String commissioni) {
		this.commissioni = commissioni;
	}
	public String getBonificoType() {
		return bonificoType;
	}
	public void setBonificoType(String bonificoType) {
		this.bonificoType = bonificoType;
	}
	public String getBeneficiario() {
		return beneficiario;
	}
	public void setBeneficiario(String beneficiario) {
		this.beneficiario = beneficiario;
	}
	public String getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	public String getHeader() {
		return header;
	}
	public void setHeader(String header) {
		this.header = header;
	}
	public String getCancel() {
		return cancel;
	}
	public void setCancel(String cancel) {
		this.cancel = cancel;
	}
	public String getCloseObjDescription() {
		return closeObjDescription;
	}
	public void setCloseObjDescription(String closeObjDescription) {
		this.closeObjDescription = closeObjDescription;
	}
	public String getFrequency() {
		return frequency;
	}
	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	
	
	public String getObjectiveName() {
		return objectiveName;
	}
	public void setObjectiveName(String objectiveName) {
		this.objectiveName = objectiveName;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getNomeObiettivo() {
		return nomeObiettivo;
	}
	public void setNomeObiettivo(String nomeObiettivo) {
		this.nomeObiettivo = nomeObiettivo;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getPayWith() {
		return payWith;
	}
	public void setPayWith(String payWith) {
		this.payWith = payWith;
	}
	public String getObjectiveNameNew() {
		return objectiveNameNew;
	}
	public void setObjectiveNameNew(String objectiveNameNew) {
		this.objectiveNameNew = objectiveNameNew;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getImporto() {
		return importo;
	}
	public void setImporto(String importo) {
		this.importo = importo;
	}
	public String getImporto2() {
		return importo2;
	}
	public void setImporto2(String importo2) {
		this.importo2 = importo2;
	}
	public String getPosteid() {
		return posteid;
	}
	public void setPosteid(String posteid) {
		this.posteid = posteid;
	}
	public String getEvolutionNumber() {
		return evolutionNumber;
	}
	public void setEvolutionNumber(String evolutionNumber) {
		this.evolutionNumber = evolutionNumber;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public String getQuickOperationName() {
		return quickOperationName;
	}
	public void setQuickOperationName(String quickOperationName) {
		this.quickOperationName = quickOperationName;
	}
	public String getQuickOperationName2() {
		return quickOperationName2;
	}
	public void setQuickOperationName2(String quickOperationName2) {
		this.quickOperationName2 = quickOperationName2;
	}
	public String getMessageText() {
		return messageText;
	}
	public void setMessageText(String messageText) {
		this.messageText = messageText;
	}
	public String getAmount2() {
		return amount2;
	}
	public void setAmount2(String amount2) {
		this.amount2 = amount2;
	}
	public String getModifiedFrequency() {
		return modifiedFrequency;
	}
	public void setModifiedFrequency(String modifiedFrequency) {
		this.modifiedFrequency = modifiedFrequency;
	}
	public String getAmount3() {
		return amount3;
	}
	public void setAmount3(String amount3) {
		this.amount3 = amount3;
	}
	
	
	
	
	
}

